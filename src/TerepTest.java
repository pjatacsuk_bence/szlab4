import junit.framework.Assert;

import org.junit.Test;

import com.bob_persie.Mezo;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.jatekkeret.Terep;


public class TerepTest {


	/**
	 * A terep tesztelese: sarkokba a NULL szomszedok, illetve egy szomszed 4 szomszedja
	 * 
	 */
	@Test
	public void test() {
		Terep terep = new Terep(7,7);
		
		//Sarkok tesztelese
		Assert.assertEquals(null,terep.getMezo(0, 0).getSzomszed(Irany.NY));
		Assert.assertEquals(null, terep.getMezo(0, 0).getSzomszed(Irany.ENY));
		
		Assert.assertEquals(null, terep.getMezo(6, 6).getSzomszed(Irany.DK));
		Assert.assertEquals(null, terep.getMezo(6, 6).getSzomszed(Irany.K));
		
		Assert.assertEquals(null, terep.getMezo(6,3).getSzomszed(Irany.K));
		Assert.assertEquals(null, terep.getMezo(6, 3).getSzomszed(Irany.EK));
		
		
		//Egyeb szomszed tesztelese
		
		
		Mezo test_mezo = terep.getMezo(4, 4);
		
		//Nyugati szomszed (x-1)
		Assert.assertEquals(terep.getMezo(3, 4),
							test_mezo.getSzomszed(Irany.NY));
		
		
		//Keleti szomszed (x+1)
		Assert.assertEquals(terep.getMezo(5, 4),
							test_mezo.getSzomszed(Irany.K));
		
		//Eszaki nyugati szomszed (y-1)
		Assert.assertEquals(terep.getMezo(3, 3),
							test_mezo.getSzomszed(Irany.ENY));
		
		//Eszak kelet szomszed (y-1)(x+1)
		Assert.assertEquals(terep.getMezo(4, 3), test_mezo.getSzomszed(Irany.EK));
		
		//Deli keleti szomszed (y+1),(x+1)
		Assert.assertEquals(terep.getMezo(4, 5),
							test_mezo.getSzomszed(Irany.DK));
		
		//Del nyugati szomszed (y+1) (x)
		Assert.assertEquals(terep.getMezo(3, 5), test_mezo.getSzomszed(Irany.DNY) );
		
		
		//3,3 paratlan teszteles
		test_mezo = terep.getMezo(3, 3);
		
		//Nyugati szomszed (x-1)
		Assert.assertEquals(terep.getMezo(2, 3),
									test_mezo.getSzomszed(Irany.NY));
				
				
		//Keleti szomszed (x+1)
		Assert.assertEquals(terep.getMezo(4, 3),
									test_mezo.getSzomszed(Irany.K));
				
		//Eszaki nyugati szomszed (y-1)
		Assert.assertEquals(terep.getMezo(3, 2),
									test_mezo.getSzomszed(Irany.ENY));
				
		//Eszak kelet szomszed (y-1)(x+1)
		Assert.assertEquals(terep.getMezo(4, 2), test_mezo.getSzomszed(Irany.EK));
		
		//Deli keleti szomszed (y+1),(x+1)
		Assert.assertEquals(terep.getMezo(4, 4),
									test_mezo.getSzomszed(Irany.DK));
				
		//Del nyugati szomszed (y+1) (x)
		Assert.assertEquals(terep.getMezo(3, 4), test_mezo.getSzomszed(Irany.DNY) );
		
	}

}
