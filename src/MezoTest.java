import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.bob_persie.JatekElem;
import com.bob_persie.Kavics;
import com.bob_persie.Mezo;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.leptetheto.Hangya;
import com.bob_persie.leptetheto.Taplalek;


public class MezoTest {
	Mezo mezo;
	
	@Before
	public void setUp(){
		mezo = new Mezo();
	}

	@Test
	public void testAddSzomszed() {
		/*
		 * Tesztelj�k a szomsz�dok hozz�ad�s�t illetve lek�r�s�t
		 * Esetek: N�gy szomsz�d felv�tele, vizsg�l�s
		 * 		   Egy m�dosit�sa vizsg�l�s
		 * 
		 * 
		 */
		Mezo keleti = new Mezo();
		Mezo nyugati = new Mezo();
		Mezo eszaki = new Mezo();
		Mezo deli = new Mezo();
		
		Mezo masik_nyugati = new Mezo();
		
		mezo.setSzomszed(Irany.DK, deli);
		
		mezo.setSzomszed(Irany.ENY, eszaki);
		mezo.setSzomszed(Irany.K, keleti);
		mezo.setSzomszed(Irany.NY, nyugati);
		
		Assert.assertEquals(nyugati, mezo.getSzomszed(Irany.NY));
		Assert.assertEquals(keleti, mezo.getSzomszed(Irany.K));
		Assert.assertEquals(deli, mezo.getSzomszed(Irany.DK));
		Assert.assertEquals(eszaki, mezo.getSzomszed(Irany.ENY));
		
		mezo.setSzomszed(Irany.NY, masik_nyugati);
		Assert.assertEquals(masik_nyugati,mezo.getSzomszed(Irany.NY));
	}
	
	
	@Test
	public void testJatekElem(){
		/*	Tesztelj�k a j�t�kelem hozz�ad�s�t illetve elt�volit�s�t a list�b�l
		 * Ehhez felvesz�nk 2k�l�nb�z�, 2 azonos tipusu elemet �s t�r�lgetj�k �ket
		 * Esetek: Ha benne van t�rl�s
		 * 			Ha nincs benne, nem t�rl�nk elemet
		 * 			
		 * 
		 */
		
		
		JatekElem hangya = new Hangya();
		JatekElem taplalek = new Taplalek();
		JatekElem kavics = new Kavics();
		JatekElem hangya2 = new Hangya();
		
		mezo.addJatekElem(hangya);
		mezo.addJatekElem(taplalek);
		mezo.addJatekElem(kavics);
		mezo.addJatekElem(hangya2);
		
		Assert.assertEquals(4, mezo.getJatekElemLista().size());
		
		mezo.removeJatekElem(hangya);
		Assert.assertEquals(3, mezo.getJatekElemLista().size());
		
		mezo.removeJatekElem(taplalek);
		Assert.assertEquals(2, mezo.getJatekElemLista().size());
		
		mezo.removeJatekElem(taplalek);
		Assert.assertEquals(2, mezo.getJatekElemLista().size());
		
		mezo.removeJatekElem(kavics);
		Assert.assertEquals(1, mezo.getJatekElemLista().size());
		
		mezo.removeJatekElem(hangya2);
		Assert.assertEquals(0,mezo.getJatekElemLista().size());
		
	}

}
