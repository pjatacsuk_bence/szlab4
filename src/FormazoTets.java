import org.junit.Test;

import com.bob_persie.utility.Formazo;


public class FormazoTets {


	@Test
	public void FormazoTest() {
		Formazo.getInstance().hivasSzoveg("Hangya.leptet()");
		Formazo.getInstance().hivasSzoveg("Mezo.getJatekElemLista()");
		Formazo.getInstance().visszateresSzoveg("List<JatekElem>");
		Formazo.getInstance().hivasSzoveg("List<JatekElem>.get(0).vanUtkozes()");
		Formazo.getInstance().visszateresSzoveg("bool : true");
		String ret = Formazo.getInstance().kerdesSzovegVisszaTeressel("Milyen iranyba lepjek?","E","K","NY","D");
		
		Formazo.getInstance().print("Ir�ny " + ret);
		}
		
}


