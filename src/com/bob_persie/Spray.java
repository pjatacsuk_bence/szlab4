package com.bob_persie;

import java.awt.Point;
import java.util.HashSet;
import java.util.Set;

import com.bob_persie.Mezo.Irany;
import com.bob_persie.grafikus.JatekElemFactory;
import com.bob_persie.utility.Formazo;
import com.bob_persie.utility.Timer;


/** Spray típusoknak az abstrakt ősosztálya
*   raktározzuk benne a spray hatósugarát,tehát,hogy
*   hány mezőre terjed ki
*   és a maradék fújások számát
*/
public abstract class Spray {
	public static final int MAX_FUJAS = 10;
	protected int _hatosugar;
	protected int _maradekFujas = 10;
	

	
	public static Point mousePoisitonToCoordinate(Point p){
		Point ret = new Point(p.x,p.y);
		
		int y = p.y / JatekElemFactory.block_height;
		int x = p.x / JatekElemFactory.block_width;
		
		if(y%2!=0){
			p.x += 1;
		}
		ret.x = x;
		ret.y = y;
		
		return ret;
	}
	
	
	public static Set<Mezo> raszterizacio(Mezo aMezo, int hatosugar) {
		if(aMezo == null){
			return null;
		}
		
		Set<Mezo> kitoltendomezok = new HashSet<Mezo>();

		kitoltendomezok.add(aMezo);

		for (int i = 0; i < hatosugar; i++) {
			Set<Mezo> tempkifejtettszomszedmezok = new HashSet<Mezo>();
			for (Mezo mezo : kitoltendomezok) {
				for (Mezo.Irany irany : Mezo.Irany.values()) {
					Mezo szomszed = mezo.getSzomszed(irany);
					if (szomszed != null) {
						tempkifejtettszomszedmezok.add(szomszed);
					}
				}
			}
			kitoltendomezok.addAll(tempkifejtettszomszedmezok);
		}
		return kitoltendomezok;
	}
	
	public static Set<Point> raszterizacio(Point point,int hatosugar){
		Set<Point> kitoltendoPontok = new HashSet<Point>();
		kitoltendoPontok.add(point);
		for(int i=0;i < hatosugar; i++){
			Set<Point> kifelejtettPontok = new HashSet<Point>();
			for(Point p: kitoltendoPontok){
				for(Irany irany : Irany.values()){
					Point szomszed = Mezo.getPointSzomszed(p,irany);
					kifelejtettPontok.add(szomszed);
				}
			}
			kitoltendoPontok.addAll(kifelejtettPontok);
		}
		
		
		
		return kitoltendoPontok;
	}
	
	
	
	/**
	 * Placeholder fuggveny
	 * @param aMezo
	 */
	public void fujMezoFolott(Mezo aMezo) {
		Formazo.getInstance().hivasSzoveg("Spray.fujMezoFolott(aX,aY)");
		
		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	public int getMaradekFujas(){
		return _maradekFujas;
	}
	
	
	
}
