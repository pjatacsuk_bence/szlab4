package com.bob_persie;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.bob_persie.Mezo.Irany;
import com.bob_persie.enums.Erzekeny;
import com.bob_persie.enums.TeritesTipus;
import com.bob_persie.grafikus.rajzegysegek.RajzEgyseg;
import com.bob_persie.interfaces.Erzekenyseg;
import com.bob_persie.interfaces.Lassitas;
import com.bob_persie.interfaces.Sebzes;
import com.bob_persie.interfaces.Terites;
import com.bob_persie.interfaces.Utkozes;
import com.bob_persie.utility.Formazo;

/**
 * Absztrakt JatekElem osztaly,
 * Minden JatekElem ososztalya, default interface megvalositasokkal
 * 
 *
 */
public abstract class JatekElem implements Utkozes, Lassitas, Erzekenyseg, Terites, Sebzes, TaplalekKezeles {
	protected int _elet = 5;
	protected int _tetlensegiIdo = 1;
	protected List<Erzekeny> _erzekenysegLista = new ArrayList<Erzekeny>();
	protected boolean isHalott = false;
	public ArrayList<Mezo> _mezok = new ArrayList<Mezo>();
	
	protected RajzEgyseg rajzEgyseg = null;
	
	/**
	 * Az osszes mezorol atlepunk egy mezore.
	 * @param aMezo - a kivant mezore mozgas
	 */
	public void mozgasMindenMezorolEgyMezore(Mezo aMezo){
		Formazo.getInstance().hivasSzoveg("JatekElem.mozgas(aMezo)");
		
		for(Mezo m : _mezok){
			m.removeJatekElem(this);
		}
		_mezok.clear();
		_mezok.add(aMezo);
		
		aMezo.addJatekElem(this);
		
		
		
		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	/**
	 * Elmozgunk egyik iranyba a szomszed mezore
	 * @param irany
	 */
	public void mozgasIranyba(Irany irany){
		Formazo.getInstance().hivasSzoveg("JatekElem.mozgasIranyba(irany)");
			
			mozgasMindenMezorolEgyMezore(_mezok.get(0).getSzomszed(irany));
			if(rajzEgyseg != null){
				rajzEgyseg.mozgasIranyba(irany);
			}
			
		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	/**
	 * Hozzaadunk egy mezot a a listahoz
	 * @param aMezo
	 */
	public void addMezo(Mezo aMezo){
		Formazo.getInstance().hivasSzoveg("JatekElem.addMezo(aMezo))");
		
		_mezok.add(aMezo);
		
		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	/**
	 * Kitorlunk egy mezot a listabol
	 * @param aMezo
	 */
	public void removeMezo(Mezo aMezo){
		Formazo.getInstance().hivasSzoveg("JatekElem.removeMezo(aMezo)");
		
		_mezok.remove(aMezo);
		
		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	/**
	 * Megnezzuk van-e utkozes
	 */
	public boolean vanUtkozes() {
		Formazo.getInstance().hivasSzoveg("JatekElem.vanUtkozes()");
		
		Formazo.getInstance().visszateresSzoveg("bool : false");
		return false;
	}

	/**
	 * Visszaadja a lassitas mertek-et
	 */
	public int getLassitasMerteke() {
		Formazo.getInstance().hivasSzoveg("JatekElem.getLassitasMerteke()");
		
		Formazo.getInstance().visszateresSzoveg("int : 0");
		return 0;
	}

	/**
	 * Visszater az erzkenysegeket tartalmazo listaval
	 */
	public List<Erzekeny> getErzekenysegLista() {
		Formazo.getInstance().hivasSzoveg("JatekElem.getErzekenysegList()");
		
		Formazo.getInstance().visszateresSzoveg("List<Erzekeny> : _erzekenysegLista");
		return _erzekenysegLista;
	}

	/**
	 * Visszater azzal, hogy van-e terites
	 */
	public boolean vanTerites() {
		Formazo.getInstance().hivasSzoveg("JatekElem.vanTerites()");
		
		Formazo.getInstance().visszateresSzoveg("bool : false");
		
		return false;
	}

	/**
	 * Visszater a teritesi esely nagysagaval
	 */
	public double getTeritesEsely() {
		Formazo.getInstance().hivasSzoveg("JatekElem.getTeritesEsely()");
		
		Formazo.getInstance().visszateresSzoveg("double : 0");
		
		return 0;
	}

	/**
	 * Visszater a terites tipusval
	 */
	public TeritesTipus getTeritesTipus() {
		Formazo.getInstance().hivasSzoveg("JatekElem.getTeritesTipus()");
		
		Formazo.getInstance().visszateresSzoveg("TeritesTipus : NINCS");
		return TeritesTipus.NINCS;
	}

	/**
	 * Elvegzi a sebzes-t
	 */
	public void sebez(int aSebzesMerteke) {
		Formazo.getInstance().hivasSzoveg("JatekElem.sebez(aSebzesMerteke)");
		
		
		_elet = Math.max(_elet - aSebzesMerteke,0);
	
		Formazo.getInstance().visszateresSzoveg("void");
	}

	/**
	 * Visszater azzal, hogy van-e taplalek
	 */
	public boolean vanTaplalek() {
		Formazo.getInstance().hivasSzoveg("JatekElem.vanTaplalek()");
		
		Formazo.getInstance().visszateresSzoveg("bool : false");
		return false;
	}

	public void mentes(File out) {
		
		
	}

	public boolean getHalott() {
		return isHalott;
	}
	
	public void halal(){
		
	}
	
	/**
	 * Beallitja a rajzEgyseget
	 */
	public void setRajzEgyseg(RajzEgyseg rajzegyseg){
		rajzEgyseg = rajzegyseg;
		
	}

	public RajzEgyseg getRajzEgyseg() {
		return rajzEgyseg;
	}

	public void mentes(int x, int y, File out) {
		// TODO Auto-generated method stub
		
	}
}