package com.bob_persie.grafikus;

public class Key {

	private boolean pressed;

	public boolean isPressed() {
		return this.pressed;
	}

	/**
	 * 
	 * @param press
	 */
	public void setPressed(boolean press) {
		this.pressed = press;
	}

}