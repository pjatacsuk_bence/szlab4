package com.bob_persie.grafikus;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

public class MenuItem {

	private String szoveg;
	private Rectangle rect;
	private Point szoveg_position = null;
	private Color rect_color = Color.GRAY;
	
	public MenuItem(String szov,Rectangle rect){
		this.szoveg = szov;
		this.rect = rect;
		
	}
	
	public void setColor(Color color){
		rect_color = color;
	}
	
	
	
	/**
	 * 
	 * @param g
	 */
	public void kiRajz(Graphics g) {
		//g.drawString(szoveg, rect.x - , y)
		if(szoveg_position == null){
			szoveg_position = new Point(0,0);
			
			FontMetrics fm = g.getFontMetrics();
			int width = fm.stringWidth(szoveg);
			
			szoveg_position.x = rect.width / 2 - width / 2;
			szoveg_position.y = rect.height/2;
		}
	
		g.setColor(rect_color);
		g.fillRect(rect.x, rect.y, rect.width, rect.height);
		g.setColor(Color.orange);
		g.drawString(szoveg, rect.x + szoveg_position.x, rect.y + szoveg_position.y);
	}

	/**
	 * 
	 * @param m
	 */
	public boolean getCollosion(Point m) {
		if(m != null){
		return rect.contains(m);
		}
		return false;
	}

}