package com.bob_persie.grafikus;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.bob_persie.enums.Buttons;

public class Controller implements KeyListener,MouseListener,MouseMotionListener {
	
	private Mouse mouse;
	private Key ESC = new Key();

	
	public Controller(){
		mouse = new Mouse();
	}
	
	/**
	 * 
	 * @param arg0
	 */
	@Override
	public void keyPressed(KeyEvent arg0) {
		if(arg0.getKeyCode() == KeyEvent.VK_ESCAPE){
			ESC.setPressed(true);
		}
	}

	/**
	 * 
	 * @param arg0
	 */
	@Override
	public void  keyReleased(KeyEvent arg0) {
		if(arg0.getKeyCode() == KeyEvent.VK_ESCAPE){
			ESC.setPressed(false);
		}
	}

	/**
	 * 
	 * @param arg0
	 */
	public void mousePressed(MouseEvent arg0) {
		mouse.setPosition(arg0.getPoint());
		mouse.setButtonPressed(true);
		if(arg0.getButton() == 3 ){
			mouse.setButtonTypePressed(Buttons.RIGHT);
		} else if(arg0.getButton() ==1) {
			mouse.setButtonTypePressed(Buttons.LEFT);
		} else if(arg0.getButton() == 2) {
			mouse.setButtonTypePressed(Buttons.MIDDLE);
		}
		
	}

	/**
	 * 
	 * @param arg0
	 */
	public  void mouseReleased(MouseEvent arg0) {
		mouse.setButtonPressed(false);
	}

	/**
	 * 
	 * @param KeyCode
	 */
	public boolean getKeyPressed(int KeyCode) {
		if(KeyCode == KeyEvent.VK_ESCAPE){
			return ESC.isPressed();
		} else {
			return false;
		}
	}

	public boolean isMouseButtonPressed() {
		return mouse.isButtonPressed();
	}

	public Buttons getPressedMouseButtonType() {
		return mouse.getButtonType();
	}

	public Point getMousePosition() {
		return mouse.getPositon();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		mouse.setPosition(arg0.getPoint());
		
	}

	

	

	

}