package com.bob_persie.grafikus;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.bob_persie.JatekElem;
import com.bob_persie.Kavics;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.Spray;
import com.bob_persie.Viz;
import com.bob_persie.grafikus.rajzegysegek.HangyaLesoRajzEgyseg;
import com.bob_persie.grafikus.rajzegysegek.HangyaMeregRajzEgyseg;
import com.bob_persie.grafikus.rajzegysegek.HangyaRajzEgyseg;
import com.bob_persie.grafikus.rajzegysegek.HangyaSzagRajzEgyseg;
import com.bob_persie.grafikus.rajzegysegek.HangyabolyRajzEgyseg;
import com.bob_persie.grafikus.rajzegysegek.HangyaszSunRajzEgyseg;
import com.bob_persie.grafikus.rajzegysegek.HexagonRajzEgyseg;
import com.bob_persie.grafikus.rajzegysegek.KavicsRajzEgyseg;
import com.bob_persie.grafikus.rajzegysegek.RajzEgyseg;
import com.bob_persie.grafikus.rajzegysegek.TaplalekRajzEgyseg;
import com.bob_persie.grafikus.rajzegysegek.TaplalekSzagRajzEgyseg;
import com.bob_persie.grafikus.rajzegysegek.VizRajzEgyseg;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.jatekkeret.Terep;
import com.bob_persie.leptetheto.Hangya;
import com.bob_persie.leptetheto.HangyaMereg;
import com.bob_persie.leptetheto.HangyaSzag;
import com.bob_persie.leptetheto.Hangyaboly;
import com.bob_persie.leptetheto.Hangyaleso;
import com.bob_persie.leptetheto.Hangyaszsun;
import com.bob_persie.leptetheto.Taplalek;
import com.bob_persie.leptetheto.TaplalekSzag;



public class JatekElemFactory {

	File f_test;
	File f_etalon;
	File out;
	String out_name;
	
	
	Terep terep;
	
	
	public static final int block_width = 32;
	public static final int block_height = 32;
	
	
	/**
	 * Private default konstruktor
	 */
	private JatekElemFactory(){
		
	}
	
	/**
	 * Default konstruktor
	 * @param path : String - a parancsokat tartalmazo file
	 */
	public JatekElemFactory(String path){
		f_test = new File(path);

	}
	
	/**
	 * Soronkent feldolgozzuk a parancsokat a megadott fileban.
	 * 
	 */
	public void parancsFeldolgozas(){
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(f_test));
			String line;
			while((line = br.readLine()) != null){
				feldolgoz(line);
			}
		
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * Egy megadott sorban levo parancsokat dolgozzuk fel.
	 */
	private void feldolgoz(String line) {
		String[] params = line.split(" ");
		
		//TEREP inicializalasa parancs
		if(params[0].equals("TEREP")){
			int x = Integer.valueOf(params[1]);
			int y = Integer.valueOf(params[2]);
			createTerep(x,y);
			LeptetoLista.getInstance().clear();
		}
		
		
		//RANDOM mod beallitasa
		if(params[0].equals("RANDOM")){
			if(params[1].equals("ON")) {
				LeptetoLista.getInstance().setRandom(true);
			} else {
				LeptetoLista.getInstance().setRandom(false);
			}
		}
		
		//Jatek elem elhelyezese a palyan
		if(params[0].equals("SET")){
			set(params);
		}
		
		
	}


	/**
	 * A terep létrehozasa
	 * @param x - a terep szelessege
	 * @param y - a terep magassaga
	 */
	private void createTerep(int x, int y) {
		terep = new Terep(x,y);
		for(int coord_y=0;coord_y<y;coord_y++){
			for(int coord_x=0;coord_x<x;coord_x++){
				//RajzEgyseg hozzadasa krealasa
				int offSet = 0;
				if(coord_y%2!=0){
					offSet = 16;
				}
				HexagonRajzEgyseg hexre = new HexagonRajzEgyseg(Irany.NY,new Point(coord_x*block_width+offSet,coord_y*block_height));
				TerepRajzoloLista.instance().addRajzEgyseg(hexre);
			}
		}
		
	}

	/**
	 * Jatekelemek elhelyezese, minden elemhez kulonbozo adatok vannak
	 */
	private void set(String[] params) {
		int x = Integer.valueOf(params[1]);
		int y = Integer.valueOf(params[2]);
		
		
		//Hangya feldolgozasa
		if(params[3].equals("HANGYA")){
			int elet = Integer.valueOf(params[4]);
			boolean taplalekvan = params[5].equals("VAN");
			Irany menetirany = Irany.valueOf(params[6]);
			int lepesszam = Integer.valueOf(params[7]);
			
			
			
			
			
			createHangya(x,y,elet,taplalekvan,menetirany,lepesszam);
			
			
		}
		//Hangyamereg feldolgozasa
		if(params[3].equals("HANGYAMEREG")){
			int elet = Integer.valueOf(params[4]);
			
			
			createHangyaMereg(x,y,elet);
			
		}
		//Hangyaszag feldolgozasa
		if(params[3].equals("HANGYASZAG")){
			int elet = Integer.valueOf(params[4]);
			double terites = Double.valueOf(params[5]);
			
			
			createHangyaSzag(x,y,elet,terites);
		
		}
		//Hangyaboly feldolgozasa
		if(params[3].equals("HANGYABOLY")){
			int elet = Integer.valueOf(params[4]);
			int hkibocs = Integer.valueOf(params[5]);
			int tapszam = Integer.valueOf(params[6]);
			
			createHangyaBoly(x,y,elet,hkibocs,tapszam);
			
		}
		//Hangyaleso feldolgozasa
		if(params[3].equals("HANGYALESO")){
			int elet = Integer.valueOf(params[4]);
			int tolcsersugar = Integer.valueOf(params[5]);
			
			createHangyaLeso(x,y,elet,tolcsersugar);
			
		}
		//Hangyaszsun feldolgozasa
		if(params[3].equals("HANGYASZSUN")){
			int elet = Integer.valueOf(params[4]);
			int megevetthangyak = Integer.valueOf(params[5]);
			int ehesseg = Integer.valueOf(params[6]);
			Irany irany = Irany.valueOf(params[7]);
			
			createHangyaszsun(x,y,elet,megevetthangyak,ehesseg,irany);
			
		}
		//Taplalekszag feldolgozasa
		if(params[3].equals("TAPLALEKSZAG")){
			
			int elet = Integer.valueOf(params[4]);
			double terites =Double.valueOf(params[5]);
			
			createTaplalekSzag(x,y,elet,terites);
			
		}
		//Kavics feldolgozasa
		if(params[3].equals("KAVICS")){
			
			
			createKavics(x,y);
			
			
		}
		//Viz feldolgozasa
		if(params[3].equals("VIZ")){
			int lassitas = Integer.valueOf(params[4]);
			createViz(x,y,lassitas);
			
		}
		if(params[3].equals("TAPLALEK")){
			int elet = Integer.valueOf(params[4]);
			int sugar = Integer.valueOf(params[5]);
			
			
			createTaplalek(x,y,elet,sugar);
		
		}
		
		
	}

	/**
	 * Taplalek letrehozasa
	 * @param x - x koordinata
	 * @param y - y koordinata
	 * @param elet - a taplalak elete
	 * @param sugar - a taplalek sugara
	 */
	private void createTaplalek(int x, int y, int elet, int sugar) {
		
		//Taplalek krealasa
		Taplalek t = new Taplalek(elet,sugar);
		
		//Mezok hozzaadasa
		terep.getMezo(x, y).addJatekElem(t);
		t.addMezo(terep.getMezo(x, y));
		
		//RajzEgyseg hozzadasa krealasa
		int offSet = 0;
		if(y%2!=0){
			offSet = 16;
		}
		TaplalekRajzEgyseg tre = new TaplalekRajzEgyseg(Irany.NY,
									new Point(x*block_width+offSet,y*block_height));
		t.setRajzEgyseg(tre);
		tre.setTaplalek(t);
		TerepRajzoloLista.instance().addRajzEgyseg(tre);
		
	
			
	
	}

	private void createHangyaszsun(int x, int y, int elet, int megevetthangyak,
			int ehesseg, Irany irany) {
		
		//Hangyaszsun krealasa
		Hangyaszsun hs = new Hangyaszsun(elet,megevetthangyak,ehesseg,irany);
		
		//Mezok beallitasa
		terep.getMezo(x, y).addJatekElem(hs);
		hs.addMezo(terep.getMezo(x,y));
		
		//RajzEgyseg hozzadasa krealasa
		int offSet = 0;
		if(y%2!=0){
			offSet = 16;
		}
		HangyaszSunRajzEgyseg hsre = new HangyaszSunRajzEgyseg(irany,new Point(x*block_width+offSet,y*block_height));
		hs.setRajzEgyseg(hsre);
		TerepRajzoloLista.instance().addRajzEgyseg(hsre);
	}

	private void createHangyaLeso(int x, int y, int elet, int tolcsersugar) {
		//Hangyaleso krealasa
		Hangyaleso hl = new Hangyaleso(elet,tolcsersugar);
		
		//Mezok beallitasa
		terep.getMezo(x, y).addJatekElem(hl);
		hl.addMezo(terep.getMezo(x,y));
		
		//RajzEgyseg hozzaadasa, krealasa
		int offSet = 0;
		if(y%2!=0){
			offSet = 16;
		}
		HangyaLesoRajzEgyseg hlre = new HangyaLesoRajzEgyseg(Irany.NY,new Point(x*block_width+offSet,y*block_height));
		hl.setRajzEgyseg(hlre);
		TerepRajzoloLista.instance().addRajzEgyseg(hlre);
		
	}

	private void createHangyaBoly(int x, int y, int elet, int hkibocs,
			int tapszam) {

		//Hangyaboly krealasa
		Hangyaboly hb = new Hangyaboly(elet,hkibocs,tapszam);
		
		//Mezok beallitasa
		terep.getMezo(x, y).addJatekElem(hb);
		hb.addMezo(terep.getMezo(x,y));
		
		//RajzEgyseg hozzaadasa, krealasa
		int offSet = 0;
		if(y%2!=0){
			offSet = 16;
		}
		HangyabolyRajzEgyseg hbre = new HangyabolyRajzEgyseg(Irany.NY,new Point(x*block_width+offSet,y*block_height));
		hb.setRajzEgyseg(hbre);
		TerepRajzoloLista.instance().addRajzEgyseg(hbre);
		
	}

	private void createViz(int x, int y, int lassitas) {
		
		//Viz krealasa
		Viz v = new Viz(lassitas);
		
		//Mezok hozzaadasa
		terep.getMezo(x, y).addJatekElem(v);
		v.addMezo(terep.getMezo(x, y));
		
		//RajzEgyseg hozzaadasa, krealasa
		int offSet = 0;
		if(y%2!=0){
			offSet = 16;
		}
		VizRajzEgyseg vre = new VizRajzEgyseg(Irany.NY,new Point(x*block_width+offSet,y*block_height));
		v.setRajzEgyseg(vre);
		TerepRajzoloLista.instance().addRajzEgyseg(vre);
		
	
	}

	private void createKavics(int x, int y) {
		//Kavics krealasa
		Kavics k = new Kavics();
		
		//Mezok hozzaadasa
		terep.getMezo(x, y).addJatekElem(k);
		k.addMezo(terep.getMezo(x, y));
		
		//RajzEgyseg hozzaadasa, krealasa
				int offSet = 0;
				if(y%2!=0){
					offSet = 16;
				}
		KavicsRajzEgyseg kre = new KavicsRajzEgyseg(Irany.NY,new Point(x*block_width+offSet,y*block_height));
		k.setRajzEgyseg(kre);
		TerepRajzoloLista.instance().addRajzEgyseg(kre);
		
	}

	private void createTaplalekSzag(int x, int y, int elet, double terites) {
		
		//TaplalekSzagKrealas
		TaplalekSzag tsz = new TaplalekSzag(elet,terites);
		
		//Mezok hozzaadasa 
		terep.getMezo(x, y).addJatekElem(tsz);
		tsz.addMezo(terep.getMezo(x,y));
		
		//RajzEgyseg hozzaadasa, krealasa
		int offSet = 0;
		if(y%2!=0){
			offSet = 16;
		}
		
		TaplalekSzagRajzEgyseg tszre = new TaplalekSzagRajzEgyseg(Irany.NY,new Point(x*block_width+offSet,y*block_height));
		tsz.setRajzEgyseg(tszre);
		TerepRajzoloLista.instance().addRajzEgyseg(tszre);
	}

	private void createHangyaSzag(int x, int y, int elet, double terites) {
	
		//HangyaSzagKrealasa
		HangyaSzag hsz = new HangyaSzag(elet,terites);
		
		terep.getMezo(x, y).addJatekElem(hsz);
		hsz.addMezo(terep.getMezo(x, y));
		
		//RajzEgyseg hozzaadasa, krealasa
		int offSet = 0;
		if(y%2!=0){
			offSet = 16;
		}
		
		HangyaSzagRajzEgyseg hszre = new HangyaSzagRajzEgyseg(Irany.NY,new Point(x*block_width+offSet,y*block_height));
		hsz.setRajzEgyseg(hszre);
		TerepRajzoloLista.instance().addRajzEgyseg(hszre);
	}

	private void createHangyaMereg(int x, int y, int elet) {
		//HangyaMereg krealas
		HangyaMereg hm = new HangyaMereg(elet);
		
		//Mezok beallitasa
		hm.addMezo(terep.getMezo(x, y));
		terep.getMezo(x, y).addJatekElem(hm);
		
		//RajzEgyseg hozzaadasa, krealasa
		int offSet = 0;
		if(y%2!=0){
			offSet = 16;
		}
		HangyaMeregRajzEgyseg hmre = new HangyaMeregRajzEgyseg(Irany.NY,new Point(x*block_width+offSet,y*block_height));
		hm.setRajzEgyseg(hmre);
		TerepRajzoloLista.instance().addRajzEgyseg(hmre);
	}

	private void createHangya(int x, int y, int elet,
			boolean taplalekvan, Irany menetirany, int lepesszam) {
		//Hangya krealas
		Hangya h = new Hangya(elet,taplalekvan,menetirany,lepesszam);
		
		//Mezok elhelyezese
		h.addMezo(terep.getMezo(x, y));
		terep.getMezo(x, y).addJatekElem(h);
		
		//RajzEgyseg hozzaadasa,krealasa
		int offSet = 0;
		if(y%2!=0){
			offSet = 16;
		}
		HangyaRajzEgyseg hre = new HangyaRajzEgyseg(menetirany,new Point(x*block_width+offSet,y*block_height));
		h.setRajzEgyseg(hre);
		TerepRajzoloLista.instance().addRajzEgyseg(hre);
		
	}

	public Terep getTerep() {
		return terep;
	}

	public void frissitNonPositionedRajzEgyseg() {
		for(int y=0;y<terep.getMagassag();y++){
			int offSet = 0;
			if(y%2!=0){
				offSet = 16;
			}
			for(int x=0;x<terep.getSzelesseg();x++){
				for(JatekElem je : terep.getMezo(x, y).getJatekElemLista()){
					if(je != null){
						RajzEgyseg re = je.getRajzEgyseg();
						if(re != null){
							Point p = re.getPosition();
							if(p.x == -1 && p.y ==-1){
								//ervenytelen koordinataval jeleztuk, hogy nem tudjuk megallapitani a poziciojat a rajzEgysegnek
								p.x = x*block_width+offSet;
								p.y = y*block_height;
								TerepRajzoloLista.instance().addRajzEgyseg(re);
							}
						}
						
					}
				}
			}
		}
		TerepRajzoloLista.instance().setFrissites(false);
		
	}

	public void mentes(String out_name) {
		 out = new File(out_name);
         try {
                 if(out.exists()){
                 out.delete();
                 }
                 out.createNewFile();
         } catch (IOException e) {
                 // TODO Auto-generated catch block
                 e.printStackTrace();
         }
         terep.mentesGrafikus(out);
		
	}
	
}
