package com.bob_persie.grafikus;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import com.bob_persie.grafikus.rajzegysegek.RajzEgyseg;
import com.bob_persie.jatekkeret.Terep;

public class TerepRajzoloLista {

	private List<RajzEgyseg> rajzEgysegek;
	private boolean isFrissit = true;
	
	protected static TerepRajzoloLista instance = null;
	
	protected TerepRajzoloLista(){
		rajzEgysegek = new ArrayList<RajzEgyseg>();
	}
	
	public static TerepRajzoloLista instance(){
		if(instance == null){
			instance = new TerepRajzoloLista();
		}
		return instance;
	}
	
	
	

	/**
	 * 
	 * @param terep
	 */
	public void rajzEgysegFrissit(Terep terep) {
		
	}

	public void addRajzEgyseg(RajzEgyseg re) {
		rajzEgysegek.add(re);
		
	}
	public void removeRajzEgyseg(RajzEgyseg re){
		rajzEgysegek.remove(re);
	}

	public class TerepRajzolo{
		/**
		 * 
		 * @param g
		 */
		public void kirajzTerep(Graphics g) {
			for(int i=0;i<rajzEgysegek.size();i++){
				rajzEgysegek.get(i).kiRajz(g);
			}
		}
	}

	public void setFrissites(boolean b) {
		isFrissit = b;
		
	}
	
	public boolean getFrissites(){
		return isFrissit;
	}
}