package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import com.bob_persie.HangyaSzagIrtoSpray;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.Spray;

public class HangyaSzagIrtoSprayRajzEgyseg extends RajzEgyseg {

private final static String path = "./img/hangyaszagirtospray.png";
	Rectangle rect = null;
	
	HangyaSzagIrtoSpray hszi = null;
	
	public HangyaSzagIrtoSprayRajzEgyseg(Irany irany,Point pozicio){
		
		super(path,irany,pozicio);
		rect = new Rectangle(pozicio.x+image.getWidth()+10,pozicio.y,30,image.getHeight());
	}
	
	public void setSpray(HangyaSzagIrtoSpray h){
		hszi = h;	
	}
	
	@Override
	public void kiRajz(Graphics g){
		g.drawImage(image,pozicio.x,pozicio.y,image.getWidth(),image.getHeight(),null);
		g.setColor(Color.green);
		double magassag =  rect.height * ((double)hszi.getMaradekFujas()/(double)Spray.MAX_FUJAS);
		g.fillRect(rect.x, rect.y +(int) Math.abs(rect.height - magassag) , rect.width,(int)magassag);
		g.setColor(Color.DARK_GRAY);
		
	}

}