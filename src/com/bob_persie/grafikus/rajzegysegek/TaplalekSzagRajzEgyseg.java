package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Point;

import com.bob_persie.Mezo.Irany;

public class TaplalekSzagRajzEgyseg extends RajzEgyseg {
private final static String path = "./img/taplalekszag.png";
	
	public TaplalekSzagRajzEgyseg(Irany irany,Point pozicio){
		
		super(path,irany,pozicio);
	}
}