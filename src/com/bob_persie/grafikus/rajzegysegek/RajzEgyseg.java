package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.bob_persie.Mezo.Irany;

public abstract class RajzEgyseg {

	
	protected BufferedImage image;
	protected Point pozicio;
	protected Irany irany;

	
	/**
	 * Hibas mukodes elkerulese vegett nincs lathato default konstruktor
	 */
	protected RajzEgyseg(){
		
	}
	
	public	RajzEgyseg(String path,Irany irany,Point pozicio){
		this.pozicio = pozicio;
		this.irany = irany;
		try {
			image = ImageIO.read(new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void mozgasIranyba(Irany irany){
		switch(irany){
		case EK:
			pozicio.x += 16;
			pozicio.y -= 32;
			break;
		case NY:
			pozicio.x -= 32;
			break;
		case DK:
			pozicio.x += 16;
			pozicio.y += 32;
			break;
		case ENY:
			pozicio.x -= 16;
			pozicio.y -= 32;
			break;
		case DNY:
			pozicio.x -= 16;
			pozicio.y += 32;
			break;
		case K:
			pozicio.x += 32;
			break;
		}
	}
	
	/**
	 * 
	 * @param g
	 */
	public void kiRajz(Graphics g) {
		g.drawImage(image,pozicio.x,pozicio.y,image.getWidth(),image.getHeight(),null);
	}

	public Point getPosition() {
		return pozicio;
	}

}