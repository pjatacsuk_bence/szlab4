package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Point;

import com.bob_persie.Mezo.Irany;

public class HangyaLesoRajzEgyseg extends RajzEgyseg {
private final static String path = "./img/hangyaleso.png";
	
	public HangyaLesoRajzEgyseg(Irany irany,Point pozicio){
		
		super(path,irany,pozicio);
	}
}