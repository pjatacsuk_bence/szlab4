package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Point;

import com.bob_persie.Mezo.Irany;

public class HexagonRajzEgyseg extends RajzEgyseg {
private final static String path = "./img/hexagon.png";
	
	public HexagonRajzEgyseg(Irany irany,Point pozicio){
		
		super(path,irany,pozicio);
	}
}
