package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Point;

import com.bob_persie.Mezo.Irany;

public class HangyaMeregRajzEgyseg extends RajzEgyseg {
private final static String path = "./img/hangyamereg.png";
	
	public HangyaMeregRajzEgyseg(Irany irany,Point pozicio){
		
		super(path,irany,pozicio);
	}
}
