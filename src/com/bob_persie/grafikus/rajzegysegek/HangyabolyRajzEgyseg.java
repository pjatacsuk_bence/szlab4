package com.bob_persie.grafikus.rajzegysegek
;

import java.awt.Point;

import com.bob_persie.Mezo.Irany;

public class HangyabolyRajzEgyseg extends RajzEgyseg {
	
	private static final String path="./img/hangyaboly.png";

	public HangyabolyRajzEgyseg(Irany irany,Point pozicio){
		super(path,irany,pozicio);
	}
	
}