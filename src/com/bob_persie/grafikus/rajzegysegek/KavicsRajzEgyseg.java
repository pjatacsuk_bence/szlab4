package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Point;

import com.bob_persie.Mezo.Irany;

public class KavicsRajzEgyseg extends RajzEgyseg {
private final static String path = "./img/kavics.png";
	
	public KavicsRajzEgyseg(Irany irany,Point pozicio){
		
		super(path,irany,pozicio);
	}
}