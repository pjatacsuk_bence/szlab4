package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Point;

import com.bob_persie.Mezo.Irany;

public class VizRajzEgyseg extends RajzEgyseg {
private final static String path = "./img/viz.png";
	
	public VizRajzEgyseg(Irany irany,Point pozicio){
		
		super(path,irany,pozicio);
	}
}