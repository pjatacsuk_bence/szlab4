package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import com.bob_persie.Mezo.Irany;
import com.bob_persie.leptetheto.Taplalek;

public class TaplalekRajzEgyseg extends RajzEgyseg {
private final static String path = "./img/taplalek.png";
	
	private Taplalek taplalek = null;

	public TaplalekRajzEgyseg(Irany irany,Point pozicio){
		
		super(path,irany,pozicio);
	}
	
	@Override
	public void kiRajz(Graphics g){
		g.drawImage(image, pozicio.x, pozicio.y, image.getWidth(), image.getHeight(),null);
		
		if(taplalek != null){
			g.setColor(Color.red);
			g.drawString(taplalek.getElet(),pozicio.x+16,pozicio.y+16);
		}
	}
	
	public void setTaplalek(Taplalek t){
		taplalek = t;
	}
}