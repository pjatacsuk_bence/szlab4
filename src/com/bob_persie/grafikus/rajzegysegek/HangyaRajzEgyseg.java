package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Point;

import com.bob_persie.Mezo.Irany;

public class HangyaRajzEgyseg extends RajzEgyseg {
private final static String path = "./img/hangya.png";
	
	public HangyaRajzEgyseg(Irany irany,Point pozicio){
		
		super(path,irany,pozicio);
	}
}