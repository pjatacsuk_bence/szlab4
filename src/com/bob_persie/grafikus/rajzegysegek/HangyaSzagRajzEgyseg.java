package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Point;

import com.bob_persie.Mezo.Irany;

public class HangyaSzagRajzEgyseg extends RajzEgyseg {
	
private final static String path = "./img/hangyaszag.png";
	
	public HangyaSzagRajzEgyseg(Irany irany,Point pozicio){
		
		super(path,irany,pozicio);
	}
}