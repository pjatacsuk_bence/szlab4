package com.bob_persie.grafikus.rajzegysegek;

import java.awt.Point;

import com.bob_persie.Mezo.Irany;

public class HangyaszSunRajzEgyseg extends RajzEgyseg {
private final static String path = "./img/hangyaszsun.png";
	
	public HangyaszSunRajzEgyseg(Irany irany,Point pozicio){
		
		super(path,irany,pozicio);
	}
}