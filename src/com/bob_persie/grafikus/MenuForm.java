package com.bob_persie.grafikus;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;

import com.bob_persie.grafikus.FoAblak.GameState;

public class MenuForm implements UpdateableForm {

	private MenuItem start;
	private MenuItem PalyaTolt;
	private MenuItem PalyaMent;
	private MenuItem resume;
	private int rect_width = 300;
	private int rect_height = 75;
	private JatekElemFactory jatekElemFactory;
	public MenuForm(){
		
		int x = FoAblak.WIDTH/2 - rect_width/2;
		int y = FoAblak.HEIGHT/2 - rect_height/2;
		start = new MenuItem("Start", 
							new Rectangle(x,y-rect_height,rect_width,rect_height));
		resume = new MenuItem("Resume", 
				new Rectangle(x,y-rect_height,rect_width,rect_height));
		PalyaTolt = new MenuItem("P�lya bet�lt�s",
								  new Rectangle(x,y,rect_width,rect_height));
		PalyaMent = new MenuItem("P�lya ment�se",
									new Rectangle(x,y+rect_height,rect_width,rect_height));
		
	}
	
	/**
	 * 
	 * @param file
	 */
	public void palyaBetolt(String file) {
		jatekElemFactory = new JatekElemFactory(file);
		jatekElemFactory.parancsFeldolgozas();
	}

	/**
	 * 
	 * @param file
	 */
	public void palyaMent(String file) {
		jatekElemFactory.mentes(file);
	}

	@Override
	public void updateForm(Controller controller) {
		updateRectColors(controller);
		handleMouseClick(controller);
		
		
	}

	private void handleMouseClick(Controller controller) {
		if(controller.isMouseButtonPressed()){
			Point p = controller.getMousePosition();
			if(start.getCollosion(p)){
				FoAblak.setGameState(GameState.GAME);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if(PalyaMent.getCollosion(p)){
				palyaMent("./maps/saved/mapx.inmap");
			} else if(PalyaTolt.getCollosion(p)){
				palyaBetolt("./maps/saved/mapx.inmap");
				FoAblak.setGameState(GameState.GAME);
			}
		}
		
	}

	private void updateRectColors(Controller controller) {
		Point mouse_pos = controller.getMousePosition();
		if(start.getCollosion(mouse_pos)){
			start.setColor(Color.GRAY);
		} else {
			start.setColor(Color.DARK_GRAY);
		}
		if(PalyaTolt.getCollosion(mouse_pos)){
			PalyaTolt.setColor(Color.GRAY);
		} else {
			PalyaTolt.setColor(Color.DARK_GRAY);
		}
		if(PalyaMent.getCollosion(mouse_pos)){
			PalyaMent.setColor(Color.GRAY);
		} else {
			PalyaMent.setColor(Color.DARK_GRAY);
		}
		if(resume.getCollosion(mouse_pos)){
			resume.setColor(Color.GRAY);
		} else {
			resume.setColor(Color.DARK_GRAY);
		}
		
	}

	@Override
	public void drawForm(Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, 0, FoAblak.WIDTH+30,FoAblak.HEIGHT+30);
		if(FoAblak.getGameState() == GameState.MENU){
		start.kiRajz(g);
		} else {
			resume.kiRajz(g);
		}
		PalyaTolt.kiRajz(g);
		PalyaMent.kiRajz(g);
	}
	
	public void setJatekFactory(JatekElemFactory jf){
		jatekElemFactory = jf;
	}

}