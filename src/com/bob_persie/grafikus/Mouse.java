package com.bob_persie.grafikus;

import java.awt.Point;

import com.bob_persie.enums.Buttons;
import com.bob_persie.utility.Timer;

public class Mouse {

	private Point pozicio;
	private Buttons buttonClicked;
	private Key button;
	
	
	public Mouse(){
		button = new Key();
		buttonClicked = Buttons.NONE;
	}
	
	/**
	 * 
	 * @param press
	 */
	public void setButtonPressed(boolean press) {
		
		button.setPressed(press);
	}

	public boolean isButtonPressed() {
		
		return button.isPressed();
	}

	public Buttons getButtonType() {
		return buttonClicked;
	}

	public Point getPositon() {
		return pozicio;
	}

	/**
	 * 
	 * @param button
	 */
	public void setButtonTypePressed(Buttons button) {
		buttonClicked = button;
	}

	/**
	 * 
	 * @param point
	 */
	public void setPosition(Point point) {
		pozicio = point;
	}

}