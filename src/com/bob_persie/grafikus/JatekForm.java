package com.bob_persie.grafikus;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.bob_persie.HangyaIrtoSpray;
import com.bob_persie.HangyaSzagIrtoSpray;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.Spray;
import com.bob_persie.enums.Buttons;
import com.bob_persie.grafikus.FoAblak.GameState;
import com.bob_persie.grafikus.TerepRajzoloLista.TerepRajzolo;
import com.bob_persie.grafikus.rajzegysegek.HangyaIrtoSprayEgyseg;
import com.bob_persie.grafikus.rajzegysegek.HangyaSzagIrtoSprayRajzEgyseg;
import com.bob_persie.jatekkeret.Jatek;
import com.bob_persie.utility.Timer;

public class JatekForm implements UpdateableForm {

	private HangyaIrtoSpray his = new HangyaIrtoSpray(1);
	private HangyaSzagIrtoSpray hszis = new HangyaSzagIrtoSpray(1);
	private Jatek jatek;

	private BufferedImage hatter;
	
	
	
	private TerepRajzolo terepRajzolo;
	protected Timer spray_timer = new Timer(1000);
	protected Timer mouse_timer = new Timer(100);
	
	public  JatekForm(){
		
		
		terepRajzolo = TerepRajzoloLista.instance().new TerepRajzolo();
		
	//	jatek = new Jatek("./maps/saved/mapx.txt");
		jatek = new Jatek("./maps/map1.inmap");
		
		HangyaSzagIrtoSprayRajzEgyseg hszisre = new HangyaSzagIrtoSprayRajzEgyseg(Irany.NY, new Point(FoAblak.WIDTH-140,20));
		TerepRajzoloLista.instance().addRajzEgyseg(hszisre);
		hszisre.setSpray(hszis);
		
		HangyaIrtoSprayEgyseg hisre = new HangyaIrtoSprayEgyseg(Irany.NY, new Point(FoAblak.WIDTH-140,20+100));
		
		TerepRajzoloLista.instance().addRajzEgyseg(hisre);
		hisre.setSpray(his);
		
		 try {
			 System.out.println(System.getProperty("user.dir"));
			hatter = ImageIO.read(new File("./img/hatter.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		jatek.start();
		jatek.setPaused(true);
	}
	
	public void ujJatek() {
		
	}

	public void jatekUpdate() {
		throw new UnsupportedOperationException();
	}

	public void jatekVege() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void updateForm(Controller controller) {
		
		
		if(controller.isMouseButtonPressed()){

			
			if(jatek.getPaused()){
				jatek.setPaused(false);
				spray_timer.start();
				
			}
			if(controller.getPressedMouseButtonType() == Buttons.RIGHT){
				
			} else if(controller.getPressedMouseButtonType() == Buttons.MIDDLE) {
				if(spray_timer.isReady()){
					Point mouse = Spray.mousePoisitonToCoordinate(controller.getMousePosition());
					his.fujMezoFolott(jatek.getTerep().getMezo(mouse.x,mouse.y));
					spray_timer.start();
				}
			} else {
				if(spray_timer.isReady()){
					Point mouse = Spray.mousePoisitonToCoordinate(controller.getMousePosition());
					hszis.fujMezoFolott(jatek.getTerep().getMezo(mouse.x,mouse.y));
					spray_timer.start();
				}
			}
		}
		updateKeys(controller);
		
	}

	private void updateKeys(Controller controller) {
		if(controller.getKeyPressed(KeyEvent.VK_ESCAPE)){
			FoAblak.setGameState(GameState.RESUME_MENU);
			jatek.setPaused(true);
		}
		
	}

	@Override
	public void drawForm(Graphics g) {
	
		g.fillRect(0, 0, FoAblak.WIDTH+30,FoAblak.HEIGHT+30);
	//	g.drawImage(hatter,0,0,hatter.getWidth(),hatter.getHeight(),null);
		terepRajzolo.kirajzTerep(g);

		if(jatek.getPaused()){
			g.setColor(Color.DARK_GRAY);
			g.fillRect(FoAblak.WIDTH/2 - 200, FoAblak.HEIGHT/2 - 100,400,200);
			FontMetrics fm = g.getFontMetrics();
			int width = fm.stringWidth("Kattintson bármelyik egérgombbal a folytatáshoz!");
			g.setColor(Color.orange);
			g.drawString("Kattintson bármelyik egérgombbal a folytatáshoz!",FoAblak.WIDTH/2 -width/2, FoAblak.HEIGHT/2);
		}
		
	}

	public JatekElemFactory getJatekElemFactory() {
		return jatek.getJatekElemFactory();
	}

	
	

}