package com.bob_persie.grafikus;

import java.awt.Graphics;

public interface UpdateableForm {

	/**
	 * 
	 * @param controller
	 */
	void updateForm(Controller controller);

	/**
	 * 
	 * @param g
	 */
	void drawForm(Graphics g);

}