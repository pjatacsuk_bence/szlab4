package com.bob_persie.grafikus;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

import com.bob_persie.grafikus.FoAblak.GameState;
public class FoAblak extends Canvas implements Runnable {

	enum GameState {
		MENU,GAME,RESUME_MENU
	}
	JatekForm jatekAblak;
	MenuForm menuAblak;
	
	private static boolean running = true;
	private static GameState gameState = GameState.MENU;

	private static final String NAME = "Hangyakalamba";
	public static final int WIDTH = 800;
	public static final int HEIGHT = 632;
	
	
	private Controller controller;
	
	private JFrame frame;
	
	public FoAblak(){
		
		/*
		 * Frame beallitasok
		 */
		setMinimumSize(new Dimension(WIDTH,HEIGHT));
		setMaximumSize(new Dimension(WIDTH,HEIGHT));
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		
		frame = new JFrame(NAME);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		
		frame.add(this,BorderLayout.CENTER);
		frame.pack();
		
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		
		int x = (dim.width - WIDTH)/2 - 50;
		int y = (dim.height - HEIGHT)/2;
		frame.setLocation(x, y);
		
		
		/*
		 * Controller-be a listenerek hozzaadasa
		 */
		
		controller = new Controller();
		this.addKeyListener(controller);
		this.addMouseMotionListener(controller);
		this.addMouseListener(controller);
		
	}
	
	private void initFoAblak() {
		
		jatekAblak = new JatekForm();
		menuAblak = new MenuForm();
		
		menuAblak.setJatekFactory(jatekAblak.getJatekElemFactory());
	}

	@Override
	public  void run() {
		long lastTime = System.nanoTime();
		
		//h�ny ns jut egy tick-re, c�l a 60 fps
		double nsPerTick = 1000000000.0 / 60.0;
		
		int ticks = 0;
		int frames = 0;
		
		long lastTimer = System.currentTimeMillis();
		double delta = 0;
		while(running) {
			long now = System.nanoTime();
			
			//kisz�moljuk h�nyszor kell tickelni
			delta += (now - lastTime) / nsPerTick;
			lastTime = now;
			boolean shouldRender  = false;
			while(delta > 0) {
				ticks++;
				tick();
				delta -= 1;
				shouldRender = true;
			}
			if(shouldRender) {
				frames++;
				render();
			}
			try {
				//sleepelj�k kicsit a thread-et, hogy 
				//drasztikusan cs�kkentj�k a cpu haszn�latot
				Thread.sleep(1);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			if(System.currentTimeMillis() - lastTimer >= 1000) {
				lastTimer += 1000;
				System.out.println(frames + " " + ticks);
				frames = 0;
				ticks = 0;
			}
			
		}
	}

	private void tick() {
		switch(gameState) {
		case MENU:
		case RESUME_MENU:
			menuAblak.updateForm(controller);
		break;
		case GAME:
			jatekAblak.updateForm(controller);
			break;
		}
		
	}

	private void render() {
		BufferStrategy strat = this.getBufferStrategy();
		if(strat == null) {
			createBufferStrategy(3);
			return;
		}
		Graphics g = strat.getDrawGraphics();
	
		switch(gameState) {
		case MENU:
		case RESUME_MENU:
			menuAblak.drawForm(g);
		break;
		case GAME:
			jatekAblak.drawForm(g);
			break;
		}
		
		
		g.dispose();
		
		strat.show();
		
	}
	/** A j�t�k thread-et indit�s�t v�gzi
	 *  
	 */
	public synchronized void start() {
		running = true;
		initFoAblak();
		new Thread(this).start();
	}

	public static void setGameState(GameState game) {
		gameState = game;
		
	}

	public static GameState getGameState() {
		return gameState;
	}


}