

package com.bob_persie.esetek;

import com.bob_persie.Kavics;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.jatekkeret.Terep;
import com.bob_persie.leptetheto.Hangya;
import com.bob_persie.leptetheto.Hangyaboly;
import com.bob_persie.leptetheto.Taplalek;


/**
 * 4. Teszt eset, A Hangya elmegy a Taplalekhoz, majd a Hangyabolyba.
 *
 */
public class HangyaTaplalekHangyaBoly extends SzkeletonEset {
	
	private Hangya hangya;
	private Taplalek taplalek;
	private Hangyaboly boly;


	public HangyaTaplalekHangyaBoly(){
	
		name = "HangyaTaplalekHangyaBoly";
		
	}
	/**
	 * Feltoltjuk a terepet kavicsokkal, illetve a hangyaval es a vizzel.
	 */
	@Override
	public void init(){
		
		terep = new Terep(5,3);
		
		for(int y=0;y<5;y++){
			for(int x=0;x<3;x++){
				if(y==0 || x==0 || y==4 || x==2 ){
				terep.getMezo(y, x).addJatekElem(new Kavics());
				}
			}
		}
		
		LeptetoLista.getInstance().clear();
		hangya = new Hangya();
		taplalek = new Taplalek();
		boly = new Hangyaboly();
		
		terep.getMezo(1, 1).addJatekElem(boly);
		boly.addMezo(terep.getMezo(1, 1));
		terep.getMezo(2, 1).addJatekElem(taplalek);
		taplalek.addMezo(terep.getMezo(2, 1));
		terep.getMezo(3, 1).addJatekElem(hangya);
		hangya.addMezo(terep.getMezo(3, 1));
	}
	/**
	 * Majd meghivjuk ra a leptet fv-t.
	 */
	@Override
	public void run(){
		while(true){
			hangya.leptet();
			boly.leptet();
		}
	}

}