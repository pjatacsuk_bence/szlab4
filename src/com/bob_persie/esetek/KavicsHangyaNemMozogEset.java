

package com.bob_persie.esetek;

import com.bob_persie.Kavics;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.jatekkeret.Terep;
import com.bob_persie.leptetheto.Hangya;


/**
 * 2. Teszt eset, utkozes specialis esete, amikor a Hangya nem tud tovabbmenni.
 *
 */
public class KavicsHangyaNemMozogEset extends SzkeletonEset {
	
	private Hangya hangya;


	/**
	 * Default konstruktor
	 */
	public KavicsHangyaNemMozogEset(){
	
		name = "KavicsHangyaNemMozogEset";
		
	}
	/**
	 * Feltoltjuk a terep-et
	 */
	@Override
	public void init(){
		
		terep = new Terep(3,3);
		
		for(int y=0;y<3;y++){
			for(int x=0;x<3;x++){
				if(y==0 || x==0 || y==2 || x==2 ){
				terep.getMezo(y, x).addJatekElem(new Kavics());
				}
			}
		}
		
		
		LeptetoLista.getInstance().clear();
		hangya = new Hangya();
		
		terep.getMezo(1, 1).addJatekElem(hangya);
		hangya.addMezo(terep.getMezo(1, 1));
	}
	
	/**
	 * A hangya-t leptetjuk
	 */
	@Override
	public void run(){
		while(true){
			hangya.leptet();
		}
	}

}
