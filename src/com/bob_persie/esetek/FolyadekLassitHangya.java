// 3. Folyadek hatasanak vizsgalata.

package com.bob_persie.esetek;

import com.bob_persie.Kavics;
import com.bob_persie.Viz;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.jatekkeret.Terep;
import com.bob_persie.leptetheto.Hangya;

/**
 * 
 */
public class FolyadekLassitHangya extends SzkeletonEset {
	
	private Hangya hangya;
	private Viz viz;


	/**
	 * Default konstruktor
	 */
	public FolyadekLassitHangya(){
	
		name = "FolyadekLassitHangya";
		
	}
	/**
	 * Feltoltjuk a terepet kavicsokkal, illetve a hangyaval es a vizzel.
	 */
	@Override
	public void init(){
		
		terep = new Terep(5,3);
		
		for(int y=0;y<5;y++){
			for(int x=0;x<3;x++){
				if(y==0 || x==0 || y==4 || x==2 ){
				terep.getMezo(y, x).addJatekElem(new Kavics());
				}
			}
		}
		
		LeptetoLista.getInstance().clear();
		hangya = new Hangya();
		viz = new Viz();
		
		terep.getMezo(2, 1).addJatekElem(viz);
		viz.addMezo(terep.getMezo(2, 1));
		terep.getMezo(3, 1).addJatekElem(hangya);
		hangya.addMezo(terep.getMezo(3, 1));
	}
	
	/**
	 * Majd meghivjuk ra a leptet fv-t.
	 */
	@Override
	public void run(){
		while(true){
			hangya.leptet();
		}
	}

}