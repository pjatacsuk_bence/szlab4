
package com.bob_persie.esetek;
import com.bob_persie.Kavics;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.jatekkeret.Terep;
import com.bob_persie.leptetheto.Hangya;


/**
 * 1. Teszt eset, Kavicsokkal korbevett Hangya az utkozes vizsgalatara.
 *
 */
public class KavicsHangyaMozogEset extends SzkeletonEset {
	
	private Hangya hangya;


	/**
	 * Default konstruktor
	 */
	public KavicsHangyaMozogEset(){
	
		name = "KavicsHangyaMozogEset";
		
	}
	
	/**
	 * Terep feltoltese a jatek elemekkel.
	 */
	@Override
	public void init(){
		
		terep = new Terep(4,3);
		
		for(int y=0;y<4;y++){
			for(int x=0;x<3;x++){
				if(y==0 || x==0 || y==3 || x==2 ){
				terep.getMezo(y, x).addJatekElem(new Kavics());
				}
			}
		}
		
		
		LeptetoLista.getInstance().clear();
		hangya = new Hangya();
		
		terep.getMezo(2, 1).addJatekElem(hangya);
		hangya.addMezo(terep.getMezo(2, 1));
	}
	
	/**
	 * Meghivjuk a hangya leptet fuggvenyet.
	 */
	@Override
	public void run(){
		while(true){
			hangya.leptet();
		}
	}

}
