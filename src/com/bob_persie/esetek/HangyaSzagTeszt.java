
package com.bob_persie.esetek;

import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.jatekkeret.Terep;
import com.bob_persie.leptetheto.Hangya;
import com.bob_persie.leptetheto.HangyaSzag;

/**
 * 
 *5. Teszt eset, A Hangyaszag hatasanak vizsgalata a Hangyara. Hangyaszagirto spray tesztelese.
 *
 */
public class HangyaSzagTeszt extends SzkeletonEset {
	
	private Hangya hangya;
	
	/**
	 * Default konstruktor
	 */
	public HangyaSzagTeszt(){
	
		name = "HangyaSzagTeszt";
		
	}
	/**
	 * A terep feltoltese hangyaszaggal es hangyaval.
	 */
	@Override
	public void init(){
		
		terep = new Terep(5,5);
		
		terep.getMezo(0, 0).addJatekElem(new HangyaSzag());
		terep.getMezo(0, 2).addJatekElem(new HangyaSzag());
		terep.getMezo(0, 4).addJatekElem(new HangyaSzag());
		terep.getMezo(2, 0).addJatekElem(new HangyaSzag());
		terep.getMezo(2, 2).addJatekElem(new HangyaSzag());
		terep.getMezo(2, 4).addJatekElem(new HangyaSzag());
		
		
		LeptetoLista.getInstance().clear();
		hangya = new Hangya();
		
		
		
		terep.getMezo(4, 2).addJatekElem(hangya);
		hangya.addMezo(terep.getMezo(4, 2));
	}
	/**
	 * Majd meghivjuk ra a leptet fv-t.
	 */
	@Override
	public void run(){
		while(true){
			hangya.leptet();
		}
	}

}