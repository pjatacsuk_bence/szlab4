
package com.bob_persie.esetek;
import com.bob_persie.Kavics;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.jatekkeret.Terep;
import com.bob_persie.leptetheto.Hangya;
import com.bob_persie.leptetheto.Hangyaszsun;

/**
 * 7. Teszt eset, A Hangyaszsun vegigrohan a palyan a Hangya fele.
 *
 */
public class HangyaszsunRohangal extends SzkeletonEset{

	private Hangya hangya;
	private Hangyaszsun sun;
	
		public HangyaszsunRohangal(){
		
			name = "HangyaszsunRohangal";
			
		}
		
		/**
		 * Feltoltjuk a terepet a jatek elemekkel.
		 */
		@Override
		public void init(){
			
			terep = new Terep(5,5);
			
			for(int y=0;y<5;y++){
				for(int x=0;x<5;x++){
					if(y==0 || x==0 || y==4 || x==4 ){
					terep.getMezo(y, x).addJatekElem(new Kavics());
					}
				}
			}
			
			
			
			LeptetoLista.getInstance().clear();
			hangya = new Hangya();
			sun=new Hangyaszsun();
			
			terep.getMezo(1, 1).addJatekElem(hangya);
			hangya.addMezo(terep.getMezo(1, 1));
			
			terep.getMezo(3,3).addJatekElem(sun);
			sun.addMezo(terep.getMezo(3, 3));
			
		}
			
		/**
		 * Majd meghivjuk a sun leptet fuggvenyet.
		 */
		@Override
		public void run(){
			while(true){
				
				sun.leptet();
				//sun.mozgasIranyba(Ny);
				
				
			}
		}

	
}
