
package com.bob_persie.esetek;
import com.bob_persie.Kavics;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.jatekkeret.Terep;
import com.bob_persie.leptetheto.Hangya;
import com.bob_persie.leptetheto.Hangyaleso;

/**
 * 8. Teszt eset, A Hangya belemegy egy Hangyaleso tolcserbe.
 *
 */
public class TolcserbeMegyHangya extends SzkeletonEset{

	private Hangya hangya;
	private Hangyaleso tolcser;
	
		public TolcserbeMegyHangya(){
		
			name = "TolcserbeMegyHangya";
			
		}
		
		/**
		 * Feltoltjuk a terepet a jatek elemekkel.
		 */
		@Override
		public void init(){
			
			terep = new Terep(4,4);
			
			for(int y=0;y<4;y++){
				for(int x=0;x<4;x++){
					if(y==0 || x==0 || y==3 || x==3 ){
					terep.getMezo(y, x).addJatekElem(new Kavics());
					}
				}
			}
			
			
			
			LeptetoLista.getInstance().clear();
			hangya = new Hangya();
			tolcser=new Hangyaleso();
			
			terep.getMezo(1, 1).addJatekElem(hangya);
			hangya.addMezo(terep.getMezo(1, 1));
			
			terep.getMezo(2,2).addJatekElem(tolcser);
			tolcser.addMezo(terep.getMezo(2, 2));
			
		}
		
		/**
		 * Meghivjuk a leptet fuggvenyeket,
		 */
		@Override
		public void run(){
			while(true){
				hangya.leptet();
				tolcser.leptet();
				
				
			}
		}

	
}
