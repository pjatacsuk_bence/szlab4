
package com.bob_persie.esetek;

import com.bob_persie.jatekkeret.Terep;

/**
 * 
 * A teszt esetek ososztalya.
 *
 */
public abstract class SzkeletonEset {
	
	protected Terep terep;
	public String name = "Unnamed eset";
	
	/**
	 * Ures abstract method
	 */
	public void init() {
	}
	
	/**
	 * Ures abstract method
	 */
	public void run() {
	}
}
