

package com.bob_persie.esetek;

import com.bob_persie.Kavics;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.jatekkeret.Terep;
import com.bob_persie.leptetheto.Hangya;
import com.bob_persie.leptetheto.TaplalekSzag;


/**
 * 6. Teszt eset, A Taplalekszag hatasanak vizsgalata a Hangyara, mikozben van nala taplalek es amikor nincs. Hangyairto spray tesztelese.
 *
 */
public class TaplalekSzagTeszt extends SzkeletonEset {
	
	private Hangya hangya;

	public TaplalekSzagTeszt(){
	
		name = "TaplalekSzagTeszt";
		
	}
	/**
	 * Feltoltjuk a terepet kavicsokkal, illetve a hangyaval es a vizzel.
	 */
	@Override
	public void init(){
		
		terep = new Terep(5,5);
		
		terep.getMezo(0, 0).addJatekElem(new TaplalekSzag());
		terep.getMezo(0, 2).addJatekElem(new TaplalekSzag());
		terep.getMezo(0, 4).addJatekElem(new TaplalekSzag());
		terep.getMezo(2, 0).addJatekElem(new TaplalekSzag());
		terep.getMezo(2, 2).addJatekElem(new TaplalekSzag());
		terep.getMezo(2, 4).addJatekElem(new TaplalekSzag());
		
		
		LeptetoLista.getInstance().clear();
		hangya = new Hangya();
		
		
		
		terep.getMezo(4, 2).addJatekElem(hangya);
		hangya.addMezo(terep.getMezo(4, 2));
	}
	/**
	 * Majd meghivjuk ra a leptet fv-t.
	 */
	@Override
	public void run(){
		while(true){
			hangya.leptet();
		}
	}

}