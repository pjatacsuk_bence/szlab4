package com.bob_persie;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.bob_persie.utility.Formazo;
/**
 * Víz nevű akadály 
 * 
 *
 */
public class Viz extends JatekElem {

	int _lassitas = 2;
	
	public Viz(int lassitas) {
		_lassitas = lassitas; 
	}

	public Viz() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Visszater a lassitas mertekevel: jelentesetben ez 2
	 */
	public int getLassitasMerteke() {
		Formazo.getInstance().hivasSzoveg("Viz.getLassitasMerteke()");
		
		Formazo.getInstance().visszateresSzoveg("int : 2");
		return _lassitas;
	}
	
	@Override
	public void mentes(File out){
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("VIZ"+" "+_lassitas);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void mentes(int x,int y,File out){
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("SET "+x+" "+y+"VIZ"+" "+_lassitas);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
