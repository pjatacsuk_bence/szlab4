package com.bob_persie;

import java.util.HashSet;
import java.util.Set;

import com.bob_persie.enums.Erzekeny;
import com.bob_persie.utility.Formazo;

/**
 * 
 * Hangyaszagírtóspray implementálása, amely egy spray típus
 * 
 */
public class HangyaSzagIrtoSpray extends Spray {

        
        
        public HangyaSzagIrtoSpray(int radius) {
                _hatosugar = radius;
        }
        
        
        /**
         * Egy megadott területen eltávolítja a hangyaszagot .
         * 
         * @param aMezo
         *            : Mezo hogy melyik mezőről távolítsuk el a szagot
         */
        public void fujMezoFolott(Mezo aMezo) {
               if(aMezo==null){
            	   //hiba
            	   return;
               }
               
               
        		Formazo.getInstance()
                                .hivasSzoveg("HangyaSzagIrtoSpray.fujMezoFolott()");

                Formazo.getInstance().hivasSzoveg(
                                "HangyaIrtoSpray.fujMezoFolott(aMezo)");

                if(_maradekFujas>0){
	                _maradekFujas--;
	                
	                Set<Mezo> irtandomezok = new HashSet<Mezo>();
	                irtandomezok = raszterizacio(aMezo, _hatosugar);
	                for (Mezo mezo : irtandomezok) {
	                        for (JatekElem j : mezo.getJatekElemLista()) {
	                                for (Erzekeny e : j.getErzekenysegLista()) {
	                                        if (e.equals(Erzekeny.HANGYASZAGIRTOSPRAY)) {
	                                                j.sebez(200);
	                                        }
	                                }
	                        }
	                }
                }
                Formazo.getInstance().visszateresSzoveg("void");
        }
}