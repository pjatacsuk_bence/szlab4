package com.bob_persie.jatekkeret;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.bob_persie.Mezo;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.utility.Formazo;

/**
 * A mezok kezdeti beallitasaert felelos osztaly
 * 
 * A mezok indexenek elrendezese
 * 	   Y 
 * X   0 1 2 3 4 5
 *     1
 *     2
 *     3
 *     4
 *     5
 * @author Pjata
 *
 */
public class Terep {
	private int _szelesseg = 5;
	private int _magassag = 5;
	public Mezo[][] mezok = new Mezo[_magassag][_szelesseg];

	/**
	 * Default konstruktor
	 * @param szelesseg : int - a kivant terep szelessege (mezo darabszam), 
	 * magassag : int - a kivant terep magassaga (mezodarabszam)
	 *
	 * 
	 * Beallitja a mezok szomszedjait.
	 */
	public Terep(int szelesseg,int magassag)
	{
		Formazo.getInstance().hivasSzoveg("Terep(szelesseg,magassag)");
		_szelesseg = szelesseg;
		_magassag = magassag;
		mezok = new Mezo[_magassag][_szelesseg+1];
		//Feltoltes
		for(int y=0;y<_magassag;y++){
			for(int x=0;x<_szelesseg;x++){
				mezok[y][x] = new Mezo();
			}
		}		
		
		for(int y=0;y<_magassag;y++){
			if(y%2 != 0){
				mezok[y][_szelesseg] = new Mezo();
				//ezek szomszedainak beallitasa
				mezok[y][_szelesseg].setSzomszed(Irany.K,null);
				mezok[y][_szelesseg].setSzomszed(Irany.EK, null);
				mezok[y][_szelesseg].setSzomszed(Irany.DK, null);
				
				//utols� sor DNY
				if(y==(_magassag-1)){
					mezok[y][_szelesseg].setSzomszed(Irany.DNY, null);
				}  else {
					//tobbi DNY
					mezok[y][_szelesseg].setSzomszed(Irany.DNY, mezok[y+1][_szelesseg]);
				}
				//els� sor ENY
				if(y==0){
					mezok[y][_szelesseg].setSzomszed(Irany.ENY, null);
				} else {
					//tobbi ENY
					mezok[y][_szelesseg].setSzomszed(Irany.ENY, mezok[y-1][_szelesseg]);
				}
				
				//NY
				mezok[y][_szelesseg].setSzomszed(Irany.NY, mezok[y][_szelesseg-1]);
			}
		}
		
		//Szomszedok beallitasa
		for(int y=0;y<_magassag;y++){
			for(int x=0;x<_szelesseg;x++)
			{
				//p�ratlan sor els� mez�
				if(y%2!=0 && x==0)
				{
					mezok[y][x]=null;
				}
				else
				{
				//Eszaknyugati szomszed
				if(y==0 || x==0) {
					mezok[y][x].setSzomszed(Irany.ENY, null);
				} else if (y%2==0){
					mezok[y][x].setSzomszed(Irany.ENY, mezok[y-1][x]);
				} else {
					mezok[y][x].setSzomszed(Irany.ENY, mezok[y-1][x-1]);
				}
				
				//Delnyugati szomszed
				if(y==(_magassag-1) || (y%2==0 && x==0)) {
					mezok[y][x].setSzomszed(Irany.DNY, null);
				} else if (y%2==0){
					mezok[y][x].setSzomszed(Irany.DNY, mezok[y+1][x]);
				} else 
				{
					mezok[y][x].setSzomszed(Irany.DNY, mezok[y+1][x-1]);
				}
				
				//Nyugati szomszed
				if(x==0 || (y%2!=0 && x==1)) {
					mezok[y][x].setSzomszed(Irany.NY, null);
				} else {
					mezok[y][x].setSzomszed(Irany.NY, mezok[y][x-1]);
				}
				//Eszakkeleti szomszed
				if(y==0 || (y%2==0 && x==(_szelesseg-1))) {
					mezok[y][x].setSzomszed(Irany.EK, null);
				} else if (y%2!=0){
					mezok[y][x].setSzomszed(Irany.EK, mezok[y-1][x]);
				} else {
					mezok[y][x].setSzomszed(Irany.EK, mezok[y-1][x+1]);
				}
				//Keleti szomszed
				if(x==(_szelesseg-1)){
					mezok[y][x].setSzomszed(Irany.K, null);
				} else {
					mezok[y][x].setSzomszed(Irany.K, mezok[y][x+1]);
				}
				
				//Delkeleti szomszed
				if(y==(_magassag-1) || (y%2==0 && x==0)) {
					mezok[y][x].setSzomszed(Irany.DK,null);
				}else if (y%2!=0){
					mezok[y][x].setSzomszed(Irany.DK, mezok[y+1][x]);
				} else  {
					mezok[y][x].setSzomszed(Irany.DK, mezok[y+1][x+1]);
				}
				
				}
			}
			
		}
		Formazo.getInstance().visszateresSzoveg("none");
	}
	
	/**
	 * 
	 * @param aX : Az elerni kivant mezo X indexe
	 * @param aY : Az elerni kivant mezo Y indexe
	 * @return
	 */
	public Mezo getMezo(int aX, int aY) {
		Formazo.getInstance().hivasSzoveg("Terep.getMezo(aX,aY)");
		if(aX < 0 || aY < 0 || aX >= _szelesseg || aY >= _magassag){
			//ha ervenytelen mezot akarunk elkerni akkor null-ot dobunk vissza
			Formazo.getInstance().visszateresSzoveg("Mezo : null");
			return null;
		} else {
			
			Formazo.getInstance().visszateresSzoveg("Mezo : mezo");
		
			if(aY%2 != 0) {
				return mezok[aY][aX+1];
			} else {
			return mezok[aY][aX];
			}	
		}
	}
	public void mentes(File out) {
		for(int y=0;y<_magassag;y++){
			for(int x=0;x<_szelesseg;x++)
			{
				getMezo(x,y).mentes(out);
			}
		}	
	}
	
	public void mentesGrafikus(File out){
		
		PrintWriter output;
		try {
			output = new PrintWriter(new FileWriter(out, true));
			output.println("TEREP " + _szelesseg + " " +_magassag);
			if(LeptetoLista.getInstance().getRandom()){
				output.println("RANDOM " + "ON");
			} else {
				output.println("RANDOM " + "OFF");
			}
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int y=0;y<_magassag;y++){
			for(int x=0;x<_szelesseg;x++)
			{
				getMezo(x,y).mentes(x,y,out);
			}
		}	
	}
	
	public void halottTorles(){
		for(int y=0;y<_magassag;y++){
			for(int x=0;x<_szelesseg;x++)
			{
				getMezo(x,y).halottTorles();
			}
		}	
	}
	
	public int getSzelesseg(){
		return _szelesseg;
	}
	public int getMagassag(){
		return _magassag;
	}
	
	public void printToConsole(){
		for(int y=0;y<_magassag;y++){
			for(int x=0;x<_szelesseg;x++)
			{
				if(getMezo(x,y).getJatekElemLista().isEmpty()){
					System.out.print(" ");
				} else {
					System.out.print("X");
				}
			}
			System.out.println("");
		}	
	}
}