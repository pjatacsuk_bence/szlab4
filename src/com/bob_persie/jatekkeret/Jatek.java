package com.bob_persie.jatekkeret;

import java.util.Timer;
import java.util.TimerTask;

import com.bob_persie.grafikus.JatekElemFactory;
import com.bob_persie.grafikus.TerepRajzoloLista;
import com.bob_persie.jatekkeret.LeptetoLista.Lepteto;
import com.bob_persie.utility.Formazo;

/**
 * A Jatek class a jatek inditasat, illetve futtatasat vegzi 
 * @author Pjata
 *
 */
public class Jatek implements Runnable{
	private int _korSzam;
	private Terep terep;
	private Lepteto lepteto = LeptetoLista.getInstance().new Lepteto();
	private JatekElemFactory jatekElemFactory;
	private boolean running = false;
	
	private static boolean already_running = false;
	
	private Timer timer = new Timer();
	private boolean paused;
	
	/**
	 * Default konstruktor	
	 * @param path : String - a jatek inicializalo file eleresi utvonola
	 */
	public Jatek(String path){
		Formazo.getInstance().hivasSzoveg("Jatek()");
		
		jatekElemFactory = new JatekElemFactory(path);
		_korSzam = 0;
		jatekElemFactory.parancsFeldolgozas();
		terep = jatekElemFactory.getTerep();
		Formazo.getInstance().visszateresSzoveg("none");
	}
	/**
	 * Uj jatek inditasa
	* @param aSzelesseg : int - a kivant terep szelessege
	 *@param aMagassag : int - a kivant terep magassega
	 */
	public void ujJatek(int aSzelesseg, int aMagassag) {
		Formazo.getInstance().hivasSzoveg("Jatek.ujJatek()");
		
		terep = new Terep(aSzelesseg,aMagassag);
		_korSzam = 0;
		
		Formazo.getInstance().visszateresSzoveg("void");
	}

	/**
	 * A jatek futtatasat vegzo metodus.
	 */
	public void jatekCiklus() {
		Formazo.getInstance().hivasSzoveg("Jatek.jatekCiklus()");
		if(TerepRajzoloLista.instance().getFrissites()){
			jatekElemFactory.frissitNonPositionedRajzEgyseg();
		}
		
		lepteto.leptetMind();
		_korSzam++;
		
		terep.halottTorles();
		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	/**
	 * visszater a Terep-pel
	 * @return terep : Terep
	 */
	public Terep getTerep(){
		return terep;
	}
	public void update() {
		jatekCiklus();
		
	}
	
	public synchronized void start() {
		running  = true;
		if(!already_running){
			new Thread(this).start();
			already_running = true;
		}
	}
	@Override
	public void run() {
		long old_time = System.currentTimeMillis();
		while(running){
				if(!paused){
					long now_time = System.currentTimeMillis();
					if(now_time - 1000 >= old_time){
						update();
						System.out.println("update");
						old_time = now_time + 1000;
					} 
					try {
						//sleepelj�k kicsit a thread-et, hogy 
						//drasztikusan cs�kkentj�k a cpu haszn�latot
						Thread.sleep(1);
					} catch (InterruptedException e) {
						
						e.printStackTrace();
					}
				}
		}
		
	}
	public JatekElemFactory getJatekElemFactory() {
		return jatekElemFactory;
	}
	
	public void setPaused(boolean paused){
		this.paused = paused;
	}
	
	public boolean getPaused(){
		return paused;
	}
	
	
}