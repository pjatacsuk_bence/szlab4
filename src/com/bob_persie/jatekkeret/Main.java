package com.bob_persie.jatekkeret;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.bob_persie.esetek.FolyadekLassitHangya;
import com.bob_persie.esetek.HangyaSzagTeszt;
import com.bob_persie.esetek.HangyaTaplalekHangyaBoly;
import com.bob_persie.esetek.HangyaszsunRohangal;
import com.bob_persie.esetek.KavicsHangyaMozogEset;
import com.bob_persie.esetek.KavicsHangyaNemMozogEset;
import com.bob_persie.esetek.ProbaEset;
import com.bob_persie.esetek.SzkeletonEset;
import com.bob_persie.esetek.TaplalekSzagTeszt;
import com.bob_persie.esetek.TolcserbeMegyHangya;

/**
 * Szkeleton main class
 * @author pjatacsuk
 *
 */
public class Main {
	
	static List<SzkeletonEset> esetek = new ArrayList<SzkeletonEset>();
	
	/**
	 * Tesztesetek inicializalasa
	 */
	public static void init() {
		esetek.add(new KavicsHangyaMozogEset());
		esetek.add(new KavicsHangyaNemMozogEset());
		esetek.add(new FolyadekLassitHangya());
		esetek.add(new HangyaTaplalekHangyaBoly());
		esetek.add(new HangyaSzagTeszt());
		esetek.add(new TaplalekSzagTeszt());
		esetek.add(new HangyaszsunRohangal());
		esetek.add(new TolcserbeMegyHangya());
		
	
	}
	
	/**
	 * Main fuggveny
	 * @param args
	 */
	public static void main(String[] args) {
		init();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		//Estek kiirasa
		
		int i = 1;
		for(SzkeletonEset eset : esetek){
			System.out.println(i + ". " + eset.name);
			i++;
		}
		
		//Loop, amig nem talalunk ertelmes inputot
		while(true){
			try {
				String sz = br.readLine();
				int sorszam = Integer.parseInt(sz);
				if(sorszam >= 1 && sorszam <= esetek.size()) {
					//Vegrehajtjuk az esetet
					SzkeletonEset  actual = esetek.get(Integer.parseInt(sz)-1);
					actual.init();
					actual.run();
				} else {
					System.out.println("Hiba's bemenet! Elvart bementek: 1 <= bemenet <= " + esetek.size());
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

	}

}
