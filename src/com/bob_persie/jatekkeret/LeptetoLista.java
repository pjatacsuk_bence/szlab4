package com.bob_persie.jatekkeret;
import java.util.ArrayList;

import com.bob_persie.interfaces.Lepes;
import com.bob_persie.utility.Formazo;


/**
 * A LeptetoLista egy singleton, celja hogy minden elem feltudjon iratkozni arra a Lista
 * amint a Lepteto vegig megy. Azon elemek iratkozhatnak fell amelyek megvalositjak a Lepes interface-t
 * @author Pjata
 *
 */
public class LeptetoLista {
	private static LeptetoLista instance = null;
	private static  boolean random = false;
	protected LeptetoLista(){
		
	}
	public static LeptetoLista getInstance(){
		if(instance == null){
			instance = new LeptetoLista();
		}
		return instance;
	}
	
	
	public ArrayList<Lepes> elemek = new ArrayList<Lepes>();

	/**
	 * A listahoz hozzaadja a megfelelo elemet
	 * @param aElem : Lepes a feliratkozni kivano elem
	 */
	public void add(Lepes aElem) {
		Formazo.getInstance().hivasSzoveg("LepesLista.add(aElem)");
		
		elemek.add(aElem);
		
		Formazo.getInstance().visszateresSzoveg("void");
	}

	/**
	 * A listarol eltavolitja a megfelelo elemet
	 * 
	 * @param aElem : az eltavolitando elem
	 */
	public void remove(Lepes aElem) {
		Formazo.getInstance().hivasSzoveg("LepesLista.remove(aElem)");
		
		elemek.remove(aElem);	
		
		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	/**
	 * A listarol kitorol minden elemet
	 */
	public void clear(){
		Formazo.getInstance().hivasSzoveg("LeptetoLista.clear()");
		
		elemek.clear();
		
		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	public void setRandom(boolean b){
		random = b;
	}
	public boolean getRandom(){
		return random;
	}
	
	/**
	 * A Lepteto class vegzi magat az elemek lepteteset, celja hogy ne mindenki
	 * tudja elvegezni a leptetMind() metodust.
	 * @author Pjata
	 *
	 */
	public class Lepteto{
		public void leptetMind() {
			Formazo.getInstance().hivasSzoveg("Lepteto.leptetMind()");
			
			for(int i=0;i<elemek.size();i++){
				
				elemek.get(i).leptet();
			}
			
			Formazo.getInstance().visszateresSzoveg("void");
		}
	}
	
	
}