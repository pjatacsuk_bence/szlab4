package com.bob_persie.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Seged osztaly a szekvencia diagramok megjelenitesere console-on
 * @author pjatacsuk
 *
 */
public class Formazo {
	private static Formazo instance;
	
	
	
	private boolean vanKiiras = false;
	private int behuzas = 0;
	protected Formazo(){
		behuzas = 0;
	}
	
	
	/**
	 * Hivas-t szimulalja, egyel noveli a behuzast
	 * @param szoveg : A Kivant szoveg pl: "Hangya.leptet()"
	 */
	public void hivasSzoveg(String szoveg){
		String final_szoveg = "";
		for(int i=0;i<behuzas;i++){
			final_szoveg += "    ";
		}
		final_szoveg += "-->";
		final_szoveg += szoveg;
		
		if(vanKiiras == true)
		System.out.println(final_szoveg);
		
		behuzas++;
		
	}
	
	/**
	 * A visszaterest szimulalja, egyel csokkenti a behuzast
	 * @param szoveg : A kivant szoveg pl: "bool : true"
	 */
	public void visszateresSzoveg(String szoveg){
		String final_szoveg = "";
		for(int i=0;i<behuzas;i++){
			final_szoveg += "    ";
		}
		
		final_szoveg += "<--";
		final_szoveg += szoveg;
		
		if(vanKiiras == true)
		System.out.println(final_szoveg);
		
		behuzas--;
		
	}
	
	/**
	 * Felteszunk egy kerdest a felhasznalotol es megadjuk milyen lehetosegeket tud valasztani
	 * Amig nem valaszt helyes inputot, addig cikusban kerdezgetjuk
	 * @param szoveg : String - A kerdes szovege
	 * @param param : String - A valaszok string formatumban
	 * @return String - A param kozul kivalasztott ervenyes lehetoseg
	 */
	public String kerdesSzovegVisszaTeressel(String szoveg,String... param){
		String final_szoveg = "";
		for(int i=0;i<behuzas;i++){
			final_szoveg += "    ";
		}
		
		final_szoveg += "[?]";
		final_szoveg += szoveg;
		
		final_szoveg += "[";
		for(String p : param){
			
			final_szoveg += p;
			final_szoveg += "/";
		}
		final_szoveg = final_szoveg.substring(0, final_szoveg.length()-1);
		final_szoveg += "]";
				
		if(vanKiiras == true) {
		System.out.println(final_szoveg);
		}
		String ret = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		
		boolean not_found = true;
		
		do{
			try {
				ret = br.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			for(String p : param){
				if(p.equals(ret)){
					not_found = false;
				}
			}
		}while(not_found);
		
		
		return ret;
	}
	
	
	/**
	 * Singleton pattern
	 * @return
	 */
	public static Formazo getInstance(){
		if(instance == null){
			instance = new Formazo();
		}
		return instance;
	}

	
	/**
	 * Egyszeru szoveg kiirasa, a megfeleo behuzassal
	 * @param szoveg
	 */
	public void print(String szoveg) {
		if(vanKiiras == false) return;
		
		String final_szoveg = "";
		for(int i=0;i<behuzas;i++){
			final_szoveg += "    ";
		}
		final_szoveg += szoveg;
		
		System.out.println(final_szoveg);
		
	}

}
