package com.bob_persie;

/**
 * A TaplalekKezeles vegzo interface
 * @author pjatacsuk
 *
 */
public interface TaplalekKezeles {
	
	/**
	 * ennek a függvénynek a segítségével vizsgálhatjuk, hogy az adott helyen van-e táplálék
	 * @return true : ha van taplalek, false ha nincs
	 */
	public boolean vanTaplalek();
}
