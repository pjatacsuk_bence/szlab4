package com.bob_persie;

import java.awt.Point;
import java.util.HashSet;
import java.util.Set;

import com.bob_persie.Mezo.Irany;
import com.bob_persie.grafikus.TerepRajzoloLista;
import com.bob_persie.grafikus.rajzegysegek.HangyaMeregRajzEgyseg;
import com.bob_persie.leptetheto.HangyaMereg;
import com.bob_persie.utility.Formazo;

/**
 * Hangya�rt�spray , amely egy spray fajta
 * 
 * 
 */
public class HangyaIrtoSpray extends Spray {

		
        
        
        public  HangyaIrtoSpray(int hatosugar){
                _hatosugar = hatosugar;
        }
        
        @Override
        /**
         * Egy megadott ter�leten hangya m�rget helyez el.
         *param�terben kapja, hogy melyik mez�re f�jjuk a m�rget
         *
         * @param aMezo : Mezo 
         */
        public void fujMezoFolott(Mezo aMezo) {
        	if(aMezo == null){
        		return;
        	}
                Formazo.getInstance().hivasSzoveg(
                                "HangyaIrtoSpray.fujMezoFolott(aMezo)");

                
                if(_maradekFujas>0){
                	_maradekFujas--;
	                Set<Mezo> mergezendomezok = new HashSet<Mezo>();
	                mergezendomezok = raszterizacio(aMezo, _hatosugar);
	   
	                
	                // ebben a cillusban m�rgezem a mez�ket
	                for (Mezo mezo : mergezendomezok) {
	                        HangyaMereg mereg = new HangyaMereg();
	                        mezo.addJatekElem(mereg);
	                        mereg.addMezo(mezo);
	                        
	                        HangyaMeregRajzEgyseg hmre = new HangyaMeregRajzEgyseg(Irany.NY, new Point(-1,-1));
	                        mereg.setRajzEgyseg(hmre);
	                        
	                        
	                }
                }
                
                TerepRajzoloLista.instance().setFrissites(true);

                Formazo.getInstance().visszateresSzoveg("void");
        }

}