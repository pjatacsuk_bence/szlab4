package com.bob_persie;
import java.awt.Point;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bob_persie.Mezo.Irany;
import com.bob_persie.utility.Formazo;


/** 
 * A Mezo felelossege a rajta levo JatekElemek es a szomszodos mezok nyilvantartasa
 * @author Pjata
 *
 */
public class Mezo {

	public enum Irany{
		
		ENY,NY,DNY,DK,K,EK;
		
		private static final Map<Integer, Irany> intToTypeMap = new HashMap<Integer,Irany>();
		static {
		    for (Irany type : Irany.values()) {
		        intToTypeMap.put(type.ordinal(), type);
		    }
		}

		public static Irany fromInt(int i) {
		    Irany type = intToTypeMap.get(Integer.valueOf(i));
		   
		    return type;
		}
		
		
		public static int getIranyMax(){
			return 5;
		}
		
		
		
		}
	
	public ArrayList<JatekElem> _JatekElemek = new ArrayList<JatekElem>();
	public Map<Irany,Mezo> szomszedok = Collections.synchronizedMap(new EnumMap<Irany,Mezo>(Irany.class));

	
	/**
	 *  
	 * @param aIrany : Irany - kivant iranyu szomszed
	 * @return szomszed : Mezo - a kivant szomszed
	 */
	public Mezo getSzomszed(Irany aIrany) {
		Formazo.getInstance().hivasSzoveg("Mezo.getSzomszed(aIrany)");
		
		Formazo.getInstance().visszateresSzoveg("Mezo : szomszed");
		return szomszedok.get(aIrany);
	}
	
	/**
	 * 
	 * @param aIrany : Irany - A be�llitand� ir�ny, aSzomszed : Mezo - az elhelyezend� szomsz�d mez�
	 * @param aSzomszed
	 */
	public void setSzomszed(Irany aIrany, Mezo aSzomszed) {
		Formazo.getInstance().hivasSzoveg("Mezo.setSzomszed(aIrany,aSzomszed)");
		
		szomszedok.put(aIrany, aSzomszed);
		
		Formazo.getInstance().visszateresSzoveg("void");
	}

	/**
	 * Visszater a mezon levo JatekElem-ekkel
	 * @return List<JatekElem> - a mezon levo jatekelemek egy listaban
	 */
	public List<JatekElem> getJatekElemLista() {
		Formazo.getInstance().hivasSzoveg("Mezo.getJatekElemLista()");
		
		Formazo.getInstance().visszateresSzoveg("List<JatekElem> : _JatekElemek");
		return _JatekElemek;
	}

	/**
	 * Hozzaadja a JatekElemeket tartalmazo listahoz az adott elemet
	 * @param aElem : JatekElem - az adott JatekElem
	 */
	public void addJatekElem(JatekElem aElem) {
		Formazo.getInstance().hivasSzoveg("Mezo.addJatekElem(aElem)");
		
		_JatekElemek.add(aElem);
		
		
		Formazo.getInstance().visszateresSzoveg("void");
	}

	/**
	 * Kitorli a JatekElemeket tartalmazo listabol az adott elemet
	 * @param aElem : JatekElem - az adott elem
	 */
	public void removeJatekElem(JatekElem aElem) {
		Formazo.getInstance().hivasSzoveg("Mezo.removeJatekElem(aElem)");
		
		Formazo.getInstance().visszateresSzoveg("void");
		_JatekElemek.remove(aElem);
	}

	public void clearJatekElemek() {
		Formazo.getInstance().hivasSzoveg("Mezo.clearJatekElemek()");
		
		Formazo.getInstance().visszateresSzoveg("void");
		_JatekElemek.clear();
		
	}

	public void mentes(File out) {
		for(JatekElem je : _JatekElemek){
			je.mentes(out);
		}
		
	}

	public void halottTorles() {
	for(int i = 0; i < _JatekElemek.size();i++){
		JatekElem je = _JatekElemek.get(i);
		if(je.getHalott()){
			je.halal();
			je.removeMezo(this);
			_JatekElemek.remove(je);
		}
		
	}
		
		
	}

	public static Point getPointSzomszed(Point p, Irany irany) {
		Point pozicio = new Point(p.x,p.y);
		
		switch(irany){
		case EK:
			if(pozicio.y%2!=0){
				pozicio.x += 1;
			}
			pozicio.y -= 1;
			break;
		case NY:
			pozicio.x -= 1;
			break;
		case DK:
			if(pozicio.y%2!=0){
				pozicio.x += 1;
			}
			pozicio.y += 1;
			break;
		case ENY:
			if(pozicio.y % 2 == 0){
				pozicio.x -= 1;	
			}
			pozicio.y -= 1;
			break;
		case DNY:
			if(pozicio.y % 2 == 0){
				pozicio.x -= 1;	
			}
			pozicio.y += 1;
			break;
		case K:
			pozicio.x += 1;
			break;
		}
		return pozicio;
	}

	public void mentes(int x, int y, File out) {
		for(JatekElem je : _JatekElemek){
			if(je.getHalott() == false){
				je.mentes(x,y,out);
			}
		}
		
	}
}