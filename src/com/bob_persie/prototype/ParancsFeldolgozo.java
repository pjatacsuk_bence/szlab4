package com.bob_persie.prototype;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.bob_persie.HangyaIrtoSpray;
import com.bob_persie.HangyaSzagIrtoSpray;
import com.bob_persie.Kavics;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.Viz;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.jatekkeret.LeptetoLista.Lepteto;
import com.bob_persie.jatekkeret.Terep;
import com.bob_persie.leptetheto.Hangya;
import com.bob_persie.leptetheto.HangyaMereg;
import com.bob_persie.leptetheto.HangyaSzag;
import com.bob_persie.leptetheto.Hangyaboly;
import com.bob_persie.leptetheto.Hangyaleso;
import com.bob_persie.leptetheto.Hangyaszsun;
import com.bob_persie.leptetheto.Taplalek;
import com.bob_persie.leptetheto.TaplalekSzag;

/**
 * A parancsfeldolgozast vegzo osztaly
 
 *
 */
public class ParancsFeldolgozo {
        
        
        
        File f_test;
        File f_etalon;
        File out;
        String out_name;
        
        Terep terep;
        
        Lepteto lepteto = LeptetoLista.getInstance().new Lepteto();
        
        /**
         * Private default konstruktor
         */
        private ParancsFeldolgozo(){
                
        }
        
        /**
         * Default konstruktor
         * @param path : String - a parancsokat tartalmazo file
         */
        public ParancsFeldolgozo(String[] args){
                f_test = new File(args[0]);
                f_etalon = new File(args[1]);
                if(f_test.isDirectory()) {
                        System.out.println("Nem file!");
                }
        
        
        
                
        }
        
        /**
         * Soronkent feldolgozzuk a parancsokat a megadott fileban.
         * 
         */
        public void parancsFeldolgozas(){
                
                try {
                        BufferedReader br = new BufferedReader(new FileReader(f_test));
                        String line;
                        while((line = br.readLine()) != null){
                                if(!line.isEmpty())
                                feldolgoz(line);
                        }
                
                        
                } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
                
                
        }

        /**
         * Egy megadott sorban levo parancsokat dolgozzuk fel.
         */
        private void feldolgoz(String line) {
                String[] params = line.split(" ");
                
                //TEREP inicializalasa parancs
                if(params[0].equals("TEREP")){
                        int x = Integer.valueOf(params[1]);
                        int y = Integer.valueOf(params[2]);
                        terep = new Terep(x,y);
                        LeptetoLista.getInstance().clear();
                }
                
                
                //RANDOM mod beallitasa
                if(params[0].equals("RANDOM")){
                        if(params[1].equals("ON")) {
                                LeptetoLista.getInstance().setRandom(true);
                        } else {
                                LeptetoLista.getInstance().setRandom(false);
                        }
                }
                
                //Jatek elem elhelyezese a palyan
                if(params[0].equals("SET")){
                        set(params);
                }
                
                //Jatek elem torlese a palyarol
                if(params[0].equals("REMOVE")){
                        int x = Integer.valueOf(params[1]);
                        int y = Integer.valueOf(params[2]);
                        
                        terep.getMezo(x, y).clearJatekElemek();
                        
                        
                }
                
                //Hangyairto spray fujasa
                if(params[0].equals("HANGYAIRTOSPRAY")){
                        int x = Integer.valueOf(params[1]);
                        int y = Integer.valueOf(params[2]);
                        int radius = Integer.valueOf(params[3]);
                        
                        HangyaIrtoSpray his = new HangyaIrtoSpray(radius);
                        his.fujMezoFolott(terep.getMezo(x, y));
                        
                }
                
                //Hangyaszag irto spray fujasa
                if(params[0].equals("HANGYASZAGIRTOSPRAY")){
                        int x = Integer.valueOf(params[1]);
                        int y = Integer.valueOf(params[2]);
                        int radius = Integer.valueOf(params[3]);
                        
                        HangyaSzagIrtoSpray hszis = new HangyaSzagIrtoSpray(radius);
                        hszis.fujMezoFolott(terep.getMezo(x, y));
                        
                        
                }
                
                //Leptet parancs feldolgozasa
                if(params[0].equals("LEPTET")){
                        lepteto.leptetMind();
                        
                        //toroljuk a listakbol a halott elemeket
                        terep.halottTorles();
                        
//                      terep.printToConsole();
                        System.out.println("");
                        
                }
                
                //kimentjuk megfelelo formatumba az adatokat
                if(params[0].equals("MENTES")){
                        out_name = params[1];
                        out = new File(out_name);
                        try {
                                if(out.exists()){
                                out.delete();
                                }
                                out.createNewFile();
                        } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                        }
                        terep.mentes(out);
                        compareToEtalon();
                }
        }

        /**
         * Az etalon file-al valo osszehasonlitas elvegzese
         */
        private void compareToEtalon() {
                String eredmeny_filenev = f_test.getName().substring(0,f_test.getName().lastIndexOf("."))+"result.out";
                File eredmeny = new File(eredmeny_filenev);
                
                try {
                        BufferedReader br_etalon = new BufferedReader(new FileReader(f_etalon));
                        BufferedReader br_eredmeny = new BufferedReader(new FileReader(out));
                        PrintWriter pw = new PrintWriter(new FileWriter(eredmeny));
                        
                        String line_etalon;
                        String line_eredmeny;
                        boolean success = true;
                        
                        //Soronkent vegig nezzuk az esetleges hibakat
                        while(((line_etalon = br_etalon.readLine())) != null && 
                                        ((line_eredmeny = br_eredmeny.readLine())) != null){
                                
                                //Ha hiba van kiirjuk, es elvesszuk a siker lehetoseget
                                if(line_eredmeny.equals(line_etalon) != true){
                                        pw.println(line_eredmeny+ "  !=  "+line_etalon);
                                        success = false;
                                }
                        }
                        if(success){
                                pw.println("SIKERES TESZT!");
                        }
                        
                        br_eredmeny.close();
                        br_etalon.close();
                        pw.close();
                        
                } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
                
        }

        /**
         * Jatekelemek elhelyezese, minden elemhez kulonbozo adatok vannak
         */
        private void set(String[] params) {
                int x = Integer.valueOf(params[1]);
                int y = Integer.valueOf(params[2]);
                
                
                //Hangya feldolgozasa
                if(params[3].equals("HANGYA")){
                        int elet = Integer.valueOf(params[4]);
                        boolean taplalekvan = params[5].equals("VAN");
                        Irany menetirany = Irany.valueOf(params[6]);
                        int lepesszam = Integer.valueOf(params[7]);
                        
                        Hangya h = new Hangya(elet,taplalekvan,menetirany,lepesszam);
                        
                        h.addMezo(terep.getMezo(x, y));
                        terep.getMezo(x, y).addJatekElem(h);
                        
                        
                        
                }
                //Hangyamereg feldolgozasa
                if(params[3].equals("HANGYAMEREG")){
                        int elet = Integer.valueOf(params[4]);
                        HangyaMereg hm = new HangyaMereg(elet);
                        
                        hm.addMezo(terep.getMezo(x, y));
                        terep.getMezo(x, y).addJatekElem(hm);
                        
                }
                //Hangyaszag feldolgozasa
                if(params[3].equals("HANGYASZAG")){
                        int elet = Integer.valueOf(params[4]);
                        double terites = Double.valueOf(params[5]);
                        HangyaSzag hsz = new HangyaSzag(elet,terites);
                        
                        terep.getMezo(x, y).addJatekElem(hsz);
                        hsz.addMezo(terep.getMezo(x, y));
                
                }
                //Hangyaboly feldolgozasa
                if(params[3].equals("HANGYABOLY")){
                        int elet = Integer.valueOf(params[4]);
                        int hkibocs = Integer.valueOf(params[5]);
                        int tapszam = Integer.valueOf(params[6]);
                        
                        Hangyaboly hb = new Hangyaboly(elet,hkibocs,tapszam);
                        
                        terep.getMezo(x, y).addJatekElem(hb);
                        hb.addMezo(terep.getMezo(x,y));
                        
                }
                //Hangyaleso feldolgozasa
                if(params[3].equals("HANGYALESO")){
                        int elet = Integer.valueOf(params[4]);
                        int tolcsersugar = Integer.valueOf(params[5]);
                        
                        Hangyaleso hl = new Hangyaleso(elet,tolcsersugar);
                        
                        terep.getMezo(x, y).addJatekElem(hl);
                        hl.addMezo(terep.getMezo(x,y));
                        
                }
                //Hangyaszsun feldolgozasa
                if(params[3].equals("HANGYASZSUN")){
                        int elet = Integer.valueOf(params[4]);
                        int megevetthangyak = Integer.valueOf(params[5]);
                        int ehesseg = Integer.valueOf(params[6]);
                        Irany irany = Irany.valueOf(params[7]);
                        
                        Hangyaszsun hs = new Hangyaszsun(elet,megevetthangyak,ehesseg,irany);
                        
                        terep.getMezo(x, y).addJatekElem(hs);
                        hs.addMezo(terep.getMezo(x,y));
                        
                }
                //Taplalekszag feldolgozasa
                if(params[3].equals("TAPLALEKSZAG")){
                        int elet = Integer.valueOf(params[4]);
                        int terites = Integer.valueOf(params[5]);
                        TaplalekSzag tsz = new TaplalekSzag(elet,terites);
                        
                        terep.getMezo(x, y).addJatekElem(tsz);
                        tsz.addMezo(terep.getMezo(x,y));
                        
                }
                //Kavics feldolgozasa
                if(params[3].equals("KAVICS")){
                        
                        Kavics k = new Kavics();
                        
                        terep.getMezo(x, y).addJatekElem(k);
                        k.addMezo(terep.getMezo(x, y));
                }
                //Viz feldolgozasa
                if(params[3].equals("VIZ")){
                        int lassitas = Integer.valueOf(params[4]);
                        
                        Viz v = new Viz(lassitas);
                        terep.getMezo(x, y).addJatekElem(v);
                        v.addMezo(terep.getMezo(x, y));
                        
                }
                if(params[3].equals("TAPLALEK")){
                        int elet = Integer.valueOf(params[4]);
                        int sugar = Integer.valueOf(params[5]);
                        
                        Taplalek t = new Taplalek(elet,sugar);
                        
                        terep.getMezo(x, y).addJatekElem(t);
                        t.addMezo(terep.getMezo(x, y));
                }
                
                
        }
}        
        
        