package com.bob_persie.interfaces;

import com.bob_persie.enums.*;

public interface Terites {
	/**
	 * Megadja, hogy a Hangyara hat-e valami terites, vagyis hogy van-e esely, hogy elforduljon a JatekElem fele.
	 */
	public boolean vanTerites();
	/**
	 * Megadja, hogy mennyi esely van arra hogy erre forduljon a JatekElem.
	 */
	public double getTeritesEsely();
	/**
	 * Megadja hogy "vonzza" vagy "taszitja" a JatekElemet.
	 */
	public TeritesTipus getTeritesTipus();
}