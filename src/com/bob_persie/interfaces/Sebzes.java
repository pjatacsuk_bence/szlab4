package com.bob_persie.interfaces;
public interface Sebzes {
	/**
	 * Az objektum _elet tulajdonsagat csokkenti aSebzesMerteke-vel. Ha az _elet<=0 a JatekElem "meghal"
	 */
	public void sebez(int aSebzesMerteke);
}