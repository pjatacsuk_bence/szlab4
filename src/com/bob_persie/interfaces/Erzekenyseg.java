package com.bob_persie.interfaces;

import com.bob_persie.enums.*;
import java.util.List;



public interface Erzekenyseg {
	/**
	 * Visszaad egy erzekenyseglistat, ami megadja, hogy mi sebezheti meg az adott JatekElemet.
	 */
	public List<Erzekeny> getErzekenysegLista();
}