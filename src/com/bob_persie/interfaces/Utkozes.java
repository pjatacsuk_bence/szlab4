package com.bob_persie.interfaces;
public interface Utkozes {
	/**
	 * Megadja hogy van-e akadaly az adott mezon. Ha van akkor erre nem lehet tovabb menni.
	 */
	public boolean vanUtkozes();
}