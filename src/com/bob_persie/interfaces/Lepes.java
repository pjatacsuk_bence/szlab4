package com.bob_persie.interfaces;
/**
 * A Lepes interfacet megvalosito objektumok leptet() fuggvenye, minden ciklusban meghivodik.
 */
public interface Lepes {
	/**
	 * A leptet fuggveny meghivasakor hajtodik vegre az oszt�lyra jellemzo viselkedes pl: Hangya->Mozog...
	 */
	public void leptet();
}