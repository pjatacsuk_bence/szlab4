package com.bob_persie.interfaces;


public interface Lassitas {
/**
 * Megadja, hogy az adott JatekElem mennyivel lassitja a Hangyat,
 *  vagyis hogy mennyi ido athaladni az adott JatekElemen.
 */
	public int getLassitasMerteke();
}