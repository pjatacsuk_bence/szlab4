package com.bob_persie.enums;

/**
 * TAVOL : a szag tavolitja az erre erzekenyeket
 * KOZEL : a szav vonza az erre erzekenyeket
 * NINCS : nincs befolyasa a szagnak
 */
public enum TeritesTipus {TAVOL,KOZEL,NINCS}
