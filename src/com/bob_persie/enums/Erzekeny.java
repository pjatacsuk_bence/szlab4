package com.bob_persie.enums;

/**
 * 
 * Az erzekenysegeket tartalmazo enum tipus.
 * Ha A erzekeny B-re, akkor B tudja sebezni A-t
 */
public enum Erzekeny {HANGYA,HANGYASZSUN,HANGYALESO,HANGYAIRTOSPRAY,HANGYASZAGIRTOSPRAY,HANGYAMEREG}