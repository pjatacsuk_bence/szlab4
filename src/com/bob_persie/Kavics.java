package com.bob_persie;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.bob_persie.utility.Formazo;

/**
 * 
 * Kavics típusú akadály a pályán
 *
 */
public class Kavics extends JatekElem {

	@Override
	/**
	 * Ezzel a függvénnyel kérjük le, hogy 
	 * a vizsgált mezőn van-e Kavics
	 *	azért tér vissza true-val, mert 
	 *	ha találkozunk Kaviccsal, azzal mindig ütközünk
	 *  
	 */
	public boolean vanUtkozes() {
		Formazo.getInstance().hivasSzoveg("Kavics.vanUtkozes()");
		
		Formazo.getInstance().visszateresSzoveg("bool : true");
		return true;
	}
	@Override
	public void mentes(File out){
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("KAVICS");
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void mentes(int x,int y,File out){
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("SET "+x+" "+y+" KAVICS");
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
