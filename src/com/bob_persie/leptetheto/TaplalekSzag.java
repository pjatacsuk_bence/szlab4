package com.bob_persie.leptetheto;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.bob_persie.JatekElem;
import com.bob_persie.enums.TeritesTipus;
import com.bob_persie.grafikus.TerepRajzoloLista;
import com.bob_persie.interfaces.Lepes;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.utility.Formazo;

public class TaplalekSzag extends JatekElem implements Lepes {
	/**
	 * ekkora esellyel fordulnak erre a Hangyak
	 */
	private double _teritesiEsely = 0.25;

	/**
	* konstruktorban hozzaadjuk a TaplalekSzagot a leptetendo
	* jatekelemek listajahoz.
	* @param teritesiEsely - ekkora esellyel fordulnak erre a Hangyak
	*/
	public TaplalekSzag(int teritesiEsely){
		Formazo.getInstance().hivasSzoveg("TaplaleSzag(teritesiEsely)");
		
		_teritesiEsely = teritesiEsely;
		
		LeptetoLista.getInstance().add(this);
		Formazo.getInstance().visszateresSzoveg("none");
	}
	
	/**
	* konstruktorban hozzaadjuk a TaplalekSzagot a leptetendo
	* jatekelemek listajahoz.
	* @param teritesiEsely - ekkora esellyel fordulnak erre a Hangyak
	*/
	public TaplalekSzag(int elet,double teritesiEsely){
		Formazo.getInstance().hivasSzoveg("TaplaleSzag(,teritesiEsely)");
		
		_elet = elet;
		_teritesiEsely = teritesiEsely;
		
		LeptetoLista.getInstance().add(this);
		Formazo.getInstance().visszateresSzoveg("none");
	}
	
	/**
	* konstruktorban hozzaadjuk a TaplalekSzagot a leptetendo
	* jatekelemek listajahoz.
	*/
	public TaplalekSzag() {
		Formazo.getInstance().hivasSzoveg("TaplalekSzag()");
		LeptetoLista.getInstance().add(this);
		Formazo.getInstance().visszateresSzoveg("none");
	}
	/**
	 * Megadja, hogy erre fordulhatnak a hangyak.
	 */
	public boolean vanTerites() {
		Formazo.getInstance().hivasSzoveg("TaplalekSzag.vanTerites()");
		
		Formazo.getInstance().visszateresSzoveg("bool : true");
		return true;
	}
	/**
	 * visszaadja, mekkora esellyel fordulnak erre a Hangyak.
	 */
	public double getTeritesEsely() {
		Formazo.getInstance().hivasSzoveg("TaplalekSzag.getTeritesEsely");
		
		Formazo.getInstance().visszateresSzoveg("double : " + _teritesiEsely);
		return _teritesiEsely;
	}
	/**
	 * Megadja, hogy erre dordulhatnak a hangyak es nem tavolodnak.
	 */
	public TeritesTipus getTeritesTipus() {
		Formazo.getInstance().hivasSzoveg("TaplalekSzag.getTeritesTipus");
		
		Formazo.getInstance().visszateresSzoveg("TeritesTipus : KOZEL");
		return TeritesTipus.KOZEL;
		
	}
	/**
	 * ez a fuggveny implementalja a hangyaszag 
	 * lepesenkent vegrehajtott cselekedeteinek az algoritmusat
	 */
	public void leptet() {
		Formazo.getInstance().hivasSzoveg("TaplalekSzag.leptet()");
		//_elet = Math.max(_elet-1, 0);
		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	@Override
	public void sebez(int sebzes){
		_elet = Math.max(_elet-sebzes, 0);
		if(_elet <= 0){
			isHalott = true;
		}
	}
	
	@Override
	public void halal(){
		TerepRajzoloLista.instance().removeRajzEgyseg(rajzEgyseg);
	}
	
	@Override
	public void mentes(File out){
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("TAPLALEKSZAG"+" "+_elet);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void mentes(int x,int y,File out){
	/*	try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("SET "+x+" "+y+" TAPLALEKSZAG"+" "+_elet+" "+_teritesiEsely);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	
}