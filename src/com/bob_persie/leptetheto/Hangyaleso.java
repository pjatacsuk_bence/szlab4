package com.bob_persie.leptetheto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.bob_persie.JatekElem;
import com.bob_persie.Mezo;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.enums.Erzekeny;
import com.bob_persie.interfaces.Lepes;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.utility.Formazo;

public class Hangyaleso extends JatekElem implements Lepes {
	private int _tolcserSugar = 2;

	/**
	* konstruktorban hozzaadjuk a hangyalesot a leptetendo
	* jatekelemek listajahoz
	*/
	public Hangyaleso(){
		Formazo.getInstance().hivasSzoveg("Hangyaleso()");
		
		LeptetoLista.getInstance().add(this);
		
		Formazo.getInstance().visszateresSzoveg("none");
	}
	/**
	* konstruktorban hozzaadjuk a hangyalesot a leptetendo
	* jatekelemek listajahoz
	* @param tolcserSugar - a tolcser sugaranak merete
	*/
	public Hangyaleso(int tolcserSugar){
		Formazo.getInstance().hivasSzoveg("Hangyaleso(tolcserSugar)");
		
		LeptetoLista.getInstance().add(this);
		
		_tolcserSugar = tolcserSugar;
		
		Formazo.getInstance().visszateresSzoveg("none");
		
		
	}
	public Hangyaleso(int elet, int tolcsersugar) {
		Formazo.getInstance().hivasSzoveg("Hangyaleso(tolcserSugar)");
		
		LeptetoLista.getInstance().add(this);
		
		_elet = elet;
		_tolcserSugar = tolcsersugar;
		
		Formazo.getInstance().visszateresSzoveg("none");
	}
	/**
	* Ez a fuggveny implementalja a hangyaleso cselekedesi algoritmusat,
	* ami minden ciklusban vegrehajtodik
	*/
	public void leptet() {
		Formazo.getInstance().hivasSzoveg("Hangyaleso.leptet()");

			
	/**
	* Megnezzuk, hogy az aktualis mezon van-e olyan JatekElem,
	* ami erzekeny a hangyalesore, ha van sebezzuk.
	*/

	for(Mezo m : _mezok){
		for(JatekElem j : m.getJatekElemLista()){
			if(j.getHalott() == false) {
				for(Erzekeny e : j.getErzekenysegLista()){
					if(e.equals(Erzekeny.HANGYALESO)){
						j.sebez(100);
					}
				}
			}
		}
	}
		
		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	@Override
	public void mentes(File out){
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("HANGYALESO"+" "+_elet+" "+_tolcserSugar);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void mentes(int x,int y,File out){
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("SET "+x+" "+y+" HANGYALESO"+" "+_elet+" "+_tolcserSugar);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}