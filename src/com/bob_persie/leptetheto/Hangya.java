package com.bob_persie.leptetheto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.bob_persie.JatekElem;
import com.bob_persie.Mezo;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.enums.Erzekeny;
import com.bob_persie.grafikus.TerepRajzoloLista;
import com.bob_persie.interfaces.Lepes;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.utility.Formazo;

public class Hangya extends JatekElem implements Lepes {
	private Irany _utirany = Irany.K;
	private boolean _taplalek = false;
	private int _lepes_szam = 0;

	/**
	 * Default konstruktor
	 * Beallitjuk az alapertekeket, erzekenysegeket
	 */
	public Hangya(){
		
		Formazo.getInstance().hivasSzoveg("Hangya()");
		_taplalek = false;
		_utirany = Irany.K;
		
		_erzekenysegLista.add(Erzekeny.HANGYAMEREG);
		_erzekenysegLista.add(Erzekeny.HANGYALESO);
		_erzekenysegLista.add(Erzekeny.HANGYASZSUN);
		
		LeptetoLista.getInstance().add(this);
		
		Formazo.getInstance().visszateresSzoveg("none");
		
	}
	
	public Hangya(int elet,boolean taplalekvan,Irany menetirany,int lepesszam){
		Formazo.getInstance().hivasSzoveg("Hangya()");
		_taplalek = taplalekvan;
		_utirany = menetirany;
        _tetlensegiIdo	 = lepesszam;
		super._elet = elet;
		_erzekenysegLista.add(Erzekeny.HANGYAMEREG);
		_erzekenysegLista.add(Erzekeny.HANGYALESO);
		_erzekenysegLista.add(Erzekeny.HANGYASZSUN);
		
		LeptetoLista.getInstance().add(this);
		
		Formazo.getInstance().visszateresSzoveg("none");
	}
	
	/**
	 * A leptetest vegzo logika.
	 */
	@Override
	public void leptet() {
		Formazo.getInstance().hivasSzoveg("Hangya.leptet()");
		
		/**
		 * Seged valtozok a terites es utkozes vizsgalathoz
		 */
		Map<Irany,Boolean> szomszedok_utkozes = Collections.synchronizedMap(new EnumMap<Irany,Boolean>(Irany.class));
		Map<Irany,Double> szomszedok_esely = Collections.synchronizedMap(new EnumMap<Irany,Double>(Irany.class));
		
		//Vegig megyunk a mezokon amiken tartozkodunk
		for(Mezo m : _mezok){
			
			
			//Megviszgaljuk, hogy ezek kozott van-e olyan tulajdonsagu ami miatt lassabbak leszunk
			int max_tetlenseg = 0;
			List<JatekElem> mezoJatekElemLista = m.getJatekElemLista();
			for(JatekElem j : mezoJatekElemLista){
				if(j.getLassitasMerteke()>max_tetlenseg){
					max_tetlenseg = j.getLassitasMerteke();
				}
			}
			_tetlensegiIdo = max_tetlenseg;
			
			//Default ertekekre beallitjuk az eselyeket
			for(Irany irany : Irany.values()){
				if(irany.equals(_utirany)){
					szomszedok_esely.put(irany,0.5);
				}else{
					szomszedok_esely.put(irany,0.0);
				}
			}
			
			//Minden iranyba megnezzuk a szomszedokat, utkozes, terites, sebzesre
			for(Irany irany : Irany.values()){
				Mezo szomszed = m.getSzomszed(irany);
				if(szomszed != null){ 
					szomszedok_utkozes.put(irany, false);
					
					
					//Lekerjuk a szomszed mezon tartozkodo jatekelemeket
					List<JatekElem> jatekElemLista = szomszed.getJatekElemLista();
					
					//Vegig megyunk ezeken
					for(JatekElem jatekElem : jatekElemLista){
						if(jatekElem != null){
							//Ha valaki erzekeny a hangya-ra, azt megsebezzuk
							for(Erzekeny e : jatekElem.getErzekenysegLista()){
								if(e.equals(Erzekeny.HANGYA)){
									jatekElem.sebez(1);
								}
							}
							
							//Utkozes vizsgalat, ha nem tudunk arra menni nem is nezzuk a taplalek / teritest
							if(jatekElem.vanUtkozes()){
								szomszedok_utkozes.put(irany, true);
							} else {
								
								//Ha nincs nalunk taplalek, akkor a szagok befolyasoljak az irany valasztast
								if(jatekElem.vanTerites() && _taplalek != true){
									switch(jatekElem.getTeritesTipus()){
									case KOZEL:
										szomszedok_esely.put(irany,
															szomszedok_esely.get(irany) + 
															jatekElem.getTeritesEsely());
										break;
									case TAVOL:
										szomszedok_esely.put(irany,
												szomszedok_esely.get(irany) - 
												jatekElem.getTeritesEsely());
										break;
									case NINCS:
										break;
									}
								}
							}
							//Ha valamelyik mezon van taplalek es a hangyanak meg nincs, akkor felvesszuk azt
							if(_taplalek != true && jatekElem.vanTaplalek()){
								_taplalek = true;
							}
						}
					}
				} else	{ 	//szomszed == null 
					
					//Ha a szomszed null, akkor ugy tekintjuk mint ha akadaly lenne ott
					szomszedok_utkozes.put(irany, true);
				}
			}//irany : Irany
			
		
		}//mezo : _Mezok
		
		//A felhasznalo szamara a szukseges informaciokat kiirjuk
		//Eselyek az iranyokra
		/*Formazo.getInstance().print("Eselyek : E -> " + szomszedok_esely.get(Irany.E) +
				   " D -> " + szomszedok_esely.get(Irany.D) +
				   " K -> " +szomszedok_esely.get(Irany.K) +
				   " NY -> " +szomszedok_esely.get(Irany.NY));

		//Utkozesek mutatasa
		Formazo.getInstance().print("Akadalyok : E - " + szomszedok_utkozes.get(Irany.E)+
				 " D - " + szomszedok_utkozes.get(Irany.D) +
				 " NY - " + szomszedok_utkozes.get(Irany.NY) +
				 " K - " + szomszedok_utkozes.get(Irany.K));*/
		
		
		
	
	
	/* LEGACY K�D SKELETONB�L
	 * 	boolean mehetunk = false;
		Irany i = _utirany;
		do{
			String menet =	Formazo.getInstance().kerdesSzovegVisszaTeressel("Milyen iranyban menjen a hangya?","E","K","D","NY");
			if(menet.equals("E")) i = Irany.E;
			if(menet.equals("NY")) i = Irany.NY;
			if(menet.equals("D")) i = Irany.D;
			if(menet.equals("K")) i = Irany.K;
			if(szomszedok_utkozes.get(i) == false){
				mehetunk = true;
			} else {
				Formazo.getInstance().print("Nem jo: arra akadaly van!");
			}
			
			
		}while(!mehetunk);*/
		
		//Eldontjuk az iranyt, ket modon: random es nem random,
		//nem random uzemmodban vegig megyunk a lehetosegeken es a legjobbat kivalasztjuk
		
		Irany i = _utirany;
		boolean vanLehetoseg = false;
		if(LeptetoLista.getInstance().getRandom() == false){
			for(Irany j : Irany.values() ){
					if(szomszedok_utkozes.get(j) == false){		
						i = j;	
						vanLehetoseg = true;
						if(szomszedok_esely.get(j) < szomszedok_esely.get(_utirany)
							&&szomszedok_utkozes.get(_utirany) == false){
							i = _utirany;
						} else {
							_utirany = i;
						}
					
					}
				}
				
			} else {
				int count = 0;
				do{
					
					Irany j = getRandomIrany();
					
						if(szomszedok_utkozes.get(j) == false){		
							i = j;	
							vanLehetoseg = true;
							if(szomszedok_esely.get(j) < szomszedok_esely.get(_utirany)
								&&szomszedok_utkozes.get(_utirany) == false){
								i = _utirany;
							} else {
								_utirany = i;
							}
					
					}
					count++;
				}while(count != 4);
			}
		
		
		
		//Sikeres iranyvalasztas eseten megvizsgaljuk tudunk-e mar lepni
		_lepes_szam++;
		if(_lepes_szam >= _tetlensegiIdo){
			
			//Ha tudunk lepni, el is mozgunk
			if(vanLehetoseg){
			
			mozgasIranyba(_utirany);
			_lepes_szam = 0;
			}
		} else {
			//Ha pedig nem, akkor kozoljuk ezt a felhasznaloval
			Formazo.getInstance().print("Lassitva vagyunk, nem tudunk meg mozogni " + (_tetlensegiIdo - _lepes_szam) + " korig");
		}
		
		
		Formazo.getInstance().visszateresSzoveg("void");
	}

	private Irany getRandomIrany() {
		//100% random, dobokockaval dobtam
		Random generator = new Random( System.currentTimeMillis() );
		int i = generator.nextInt(Irany.getIranyMax());
		return Irany.fromInt(i);
	}

	/**
	 * @return List<Erzekeny> : A hangya erzekenyseg listaja
	 */
	@Override
	public List<Erzekeny> getErzekenysegLista() {
		Formazo.getInstance().hivasSzoveg("Hangya.getErzekenysegLista()");
		
		Formazo.getInstance().visszateresSzoveg("List<Erzekeny> : erzekenysegLista");
		
		return _erzekenysegLista;
	}

	/**
	 * Feltelezzuk, hogy el is veszik a taplalekot ha van
	 * 
	 */
	@Override
	public boolean vanTaplalek() {
		Formazo.getInstance().hivasSzoveg("Hangya.vanTaplalek()");
		if(_taplalek == true){
			_taplalek = false;
			
			Formazo.getInstance().visszateresSzoveg("bool : true");
			return true;
		} else {
			
			Formazo.getInstance().visszateresSzoveg("bool : false");
			return false;
		}
	}
	
	@Override
	public void sebez(int sebzes){
		_elet = Math.max(0, _elet-sebzes);
		System.out.println("Hangya: "+_elet);
		if(_elet == 0){
			
			isHalott = true;
		}
	}
	@Override
	public void halal(){
		LeptetoLista.getInstance().remove(this);
		TerepRajzoloLista.instance().removeRajzEgyseg(rajzEgyseg);
	}
	
	
	
	@Override
	public void mentes(File out){
		try {
			String tp;
			if(_taplalek){
				tp = "VAN";
			} else {
				tp = "NINCS";
			}
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("HANGYA"+" "+_elet+" "+tp+" "+_utirany+" "+_tetlensegiIdo);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void mentes(int x,int y,File out){
		try {
			String tp;
			if(_taplalek){
				tp = "VAN";
			} else {
				tp = "NINCS";
			}
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("SET "+x+" "+y+" HANGYA"+" "+_elet+" "+tp+" "+_utirany+" "+_tetlensegiIdo);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}