package com.bob_persie.leptetheto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.bob_persie.JatekElem;
import com.bob_persie.Mezo;
import com.bob_persie.enums.Erzekeny;
import com.bob_persie.grafikus.TerepRajzoloLista;
import com.bob_persie.interfaces.Lepes;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.utility.Formazo;
/**
* Hangyamereg nevu Jatekelem implementacioja
*/

public class HangyaMereg extends JatekElem implements Lepes {
	
	public HangyaMereg(){
		LeptetoLista.getInstance().add(this);
	}
	
	public HangyaMereg(int elet) {
		_elet = elet;
		LeptetoLista.getInstance().add(this);
	}

	/**
	 * ez a fuggveny implementalja a hangyamereg 
	 * lepesenkent vegrehajtott cselekedeteinek az algoritmusat
	 */
	
	public void leptet() {
		Formazo.getInstance().hivasSzoveg("HangyaMereg.leptet()");
		
		/**
		 * megnezi, hogy az o mezojen milyen jatekelemek vannak
		 * es ha valamelyikük erzekeny a HANGYAMEREG-re
		 * akkor azt megsebzi
		 */
		for(Mezo m : _mezok){
			for(JatekElem j : m.getJatekElemLista()){
				for(Erzekeny e : j.getErzekenysegLista()){
					if(e.equals(Erzekeny.HANGYAMEREG)){
						j.sebez(25);
						System.out.println("Sebez");
					}
				}
			}
		}
		/**
		 * minden korben csokken az elete
		 * es ha elfogy, akkor eltunik
		 */
		_elet = Math.max(_elet - 1, 0);
		if(_elet == 0) {
			isHalott = true;
		}
		
		
		
		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	@Override
	public void mentes(File out){
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("HANGYAMEREG"+" "+_elet);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void mentes(int x,int y,File out){
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("SET "+x+" "+y+" HANGYAMEREG"+" "+_elet);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void halal(){
		LeptetoLista.getInstance().remove(this);
		TerepRajzoloLista.instance().removeRajzEgyseg(rajzEgyseg);
	}
}
