package com.bob_persie.leptetheto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.bob_persie.JatekElem;
import com.bob_persie.enums.Erzekeny;
import com.bob_persie.enums.TeritesTipus;
import com.bob_persie.grafikus.TerepRajzoloLista;
import com.bob_persie.interfaces.Lepes;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.utility.Formazo;

public class HangyaSzag extends JatekElem implements Lepes {
        /**
         * ekkora esellyel fordulnak erre a Hangyak
         */
        private double _teritesiEsely = 0.25;

        /**
        * konstruktorban hozzaadjuk a HangyaSzagot a leptetendo
        * jatekelemek listajahoz
        */
        public HangyaSzag(){
                Formazo.getInstance().hivasSzoveg("HangyaSzag()");
                
                _erzekenysegLista.add(Erzekeny.HANGYASZAGIRTOSPRAY);
                LeptetoLista.getInstance().add(this);
                
                Formazo.getInstance().visszateresSzoveg("none");
        }
        /**
        * konstruktorban hozzaadjuk a HangyaSzagot a leptetendo
        * jatekelemek listajahoz.
        * @param teritesiEsely - ekkora esellyel fordulnak erre a Hangyak
        */
        public HangyaSzag(double teritesiEsely){
                
        }
        public HangyaSzag(int elet, double terites) {
                Formazo.getInstance().hivasSzoveg("HangyaSzag(teritesiEsely)");
                
                _teritesiEsely = terites;
                _elet = elet;
                
                _erzekenysegLista.add(Erzekeny.HANGYASZAGIRTOSPRAY);
                
                LeptetoLista.getInstance().add(this);
                
                Formazo.getInstance().visszateresSzoveg("none");
        }
        /**
         * erre fordulhatnak a hangyak.
         */
        public boolean vanTerites() {
                Formazo.getInstance().hivasSzoveg("HangyaSzag.vanTerites()");
                
                Formazo.getInstance().visszateresSzoveg("bool : true");
                return true;
        }
        /**
         * visszaadja, mekkora esellyel fordulnak erre a Hangyak.
         */
        public double getTeritesEsely() {
                Formazo.getInstance().hivasSzoveg("HangyaSzag.getTeritesEsely()");
                
                Formazo.getInstance().visszateresSzoveg("double : " + _teritesiEsely);
                return _teritesiEsely;
        }
        /**
         * Megadja, hogy erre dordulhatnak a hangyak es nem tavolodnak.
         */
        public TeritesTipus getTeritesTipus() {
                Formazo.getInstance().hivasSzoveg("HangyaSzag.getTeritesTipus()");
                
                Formazo.getInstance().visszateresSzoveg("TeritesTipus : KOZEL");
                return TeritesTipus.KOZEL;
        }

        @Override
        public void sebez(int sebzes)
        {
                _elet = Math.max(_elet - sebzes, 0);
                if(_elet == 0) {
                        
                        isHalott = true;
                }
        }
        
        @Override
        public void halal(){
                LeptetoLista.getInstance().remove(this);
                TerepRajzoloLista.instance().removeRajzEgyseg(rajzEgyseg);
        }
        
        /**
         * ez a fuggveny implementalja a hangyaszag 
         * lepesenkent vegrehajtott cselekedeteinek az algoritmusat
         */
        public void leptet() {
                Formazo.getInstance().hivasSzoveg("HangyaSzag.leptet()");
                
                _elet = Math.max(_elet - 1, 0);
                if(_elet == 0) {
                	isHalott = true;
                }
                
                Formazo.getInstance().visszateresSzoveg("void");
                
        }
        @Override
        public void mentes(File out){
                try {
                        PrintWriter output = new PrintWriter(new FileWriter(out, true));
                        output.println("HANGYASZAG"+" "+_elet+" "+_teritesiEsely);
                        output.close();
                } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
        }
        @Override
        public void mentes(int x,int y,File out){
                try {
                        PrintWriter output = new PrintWriter(new FileWriter(out, true));
                        output.println("SET "+x+" "+y+" HANGYASZAG"+" "+_elet+" "+_teritesiEsely);
                        output.close();
                } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
        }
}