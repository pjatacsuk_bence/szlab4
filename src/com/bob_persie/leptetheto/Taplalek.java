package com.bob_persie.leptetheto;

import java.awt.Point;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.bob_persie.JatekElem;
import com.bob_persie.Mezo;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.Spray;
import com.bob_persie.grafikus.TerepRajzoloLista;
import com.bob_persie.grafikus.rajzegysegek.TaplalekSzagRajzEgyseg;
import com.bob_persie.interfaces.Lepes;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.utility.Formazo;

/**
 * 
 * T�pl�l�k nev� j�t�kelem rakt�rozzuk benne, hogy mekkora sug�rban �rz�dik a
 * szaga �s az �ltala kibocs�jtott szagokat egy list�ban
 */
public class Taplalek extends JatekElem implements Lepes {
        private int _szagSugar = 1;
        public List<TaplalekSzag> taplalekSzagok = new ArrayList<TaplalekSzag>();
        public List<TaplalekSzagRajzEgyseg> taplalekSzagRajzok;
        boolean van_szag = false;

        /**
         * Default konstruktor
         * 
         * @param szagSugar
         *            : int - a szag sugar nagysaga
         */
        public Taplalek(int szagSugar) {
                Formazo.getInstance().hivasSzoveg("Taplalek(szagSugar)");
                _szagSugar = szagSugar;
                LeptetoLista.getInstance().add(this);
                Formazo.getInstance().visszateresSzoveg("none");
        }

        /**
         * Default konstruktor
         */
        public Taplalek() {
                Formazo.getInstance().hivasSzoveg("Taplalek()");
                
                 LeptetoLista.getInstance().add(this);
                 
                Formazo.getInstance().visszateresSzoveg("none");
        }

        public void leptet() {
                Formazo.getInstance().hivasSzoveg("Taplalek.leptet()");
                if(van_szag == false){
	                Mezo aMezo = _mezok.get(0);
	                Set<Mezo> szagmezok = new HashSet<Mezo>();

	                for(int i=1;i<=_szagSugar;i++){
		                szagmezok = Spray.raszterizacio(aMezo, i);
		                
		                for (Mezo mezo : szagmezok) {
		                        TaplalekSzag szag = new TaplalekSzag(10-i);
		                      
		                        mezo.addJatekElem(szag);
		                        szag.addMezo(mezo);
		                        
		                        taplalekSzagok.add(szag);
		                        
		                        TaplalekSzagRajzEgyseg tszre = new TaplalekSzagRajzEgyseg(Irany.NY, new Point(-1,-1));
		                        szag.setRajzEgyseg(tszre);
	                
		                }
	                }
	                	van_szag = true;
	                	TerepRajzoloLista.instance().setFrissites(true);
	                }
                 Formazo.getInstance().visszateresSzoveg("void");
        }

        public Taplalek(int elet, int sugar) {
                _elet = elet;
                _szagSugar = sugar;
                 LeptetoLista.getInstance().add(this);
        }

        /**
         * ez a f�ggv�ny mondja meg, hogy van -e t�pl�l�k az �ppen vizsg�lt mez�n
         */
        @Override
        public boolean vanTaplalek() {
                Formazo.getInstance().hivasSzoveg("Taplalek.vanTaplalek()");
                // megvizsg�lja, hogy a t�pl�l�knak van-e m�g �lete
                // ha van akkor van t�pl�l�k a mez�n �s cs�kkenti az �let�t
                if (_elet >	 0) {
                        _elet--;
                        Formazo.getInstance().visszateresSzoveg("bool : true");
                        return true;
                } else {
                		isHalott = true;
                        Formazo.getInstance().visszateresSzoveg("bool : false");
                        return false;
                }

        }

        public void setTaplalekRajzok(List<TaplalekSzagRajzEgyseg> tszagok){
        	taplalekSzagRajzok = tszagok;
        }
        
        @Override
        public void halal(){
        	LeptetoLista.getInstance().remove(this);
        	TerepRajzoloLista.instance().removeRajzEgyseg(rajzEgyseg);
        	
        
        	for(int i=0;i<taplalekSzagok.size();i++){
        		taplalekSzagok.get(i).sebez(200);
        	}
        }
        
        @Override
        public void mentes(File out) {
                try {
                        PrintWriter output = new PrintWriter(new FileWriter(out, true));
                        output.println("TAPLALEK" + " " + _elet + " " + _szagSugar);
                        output.close();
                } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
        }
        
        @Override
        public void mentes(int x,int y,File out) {
                try {
                        PrintWriter output = new PrintWriter(new FileWriter(out, true));
                        output.println("SET "+x+" "+y+" TAPLALEK" + " " + _elet + " " + _szagSugar);
                        output.close();
                } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
        }

		public String getElet() {
			return String.valueOf(_elet);
		}
}