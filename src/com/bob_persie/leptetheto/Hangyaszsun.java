package com.bob_persie.leptetheto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import com.bob_persie.JatekElem;
import com.bob_persie.Kavics;
import com.bob_persie.Mezo;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.enums.Erzekeny;
import com.bob_persie.grafikus.TerepRajzoloLista;
import com.bob_persie.interfaces.Lepes;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.utility.Formazo;
/**
 * Hangyaszsun nevu jatekelem implementacioja
 * raktarozza az altala megevett hangyak szamat,
 * annak a merteket, hogy mennyire ehes
 * es milyen iranyba tart
 */
public class Hangyaszsun extends JatekElem implements Lepes {
	private int _megevettHangyak = 0;
	private int _ehesseg = 3;
	private Irany _utirany = Irany.K;

	public Hangyaszsun() {
		Formazo.getInstance().hivasSzoveg("Hangyaszsun()");
		/**
		 * hozzaadjuk a leptetendo jatekelemek listajahoz
		 */
		LeptetoLista.getInstance().add(this);

		//Formazo.getInstance().visszateresSzovaElemeg("none");
	}

	public Hangyaszsun(int ehesseg) {
		Formazo.getInstance().hivasSzoveg("Hangyaszsun(ehesseg)");

		_ehesseg = ehesseg;
		LeptetoLista.getInstance().add(this);

		Formazo.getInstance().visszateresSzoveg("none");
	}

	public Hangyaszsun(int elet, int megevetthangyak, int ehesseg, Irany irany) {
		Formazo.getInstance().hivasSzoveg("Hangyaszsun(ehesseg)");

		_elet = elet;
		_megevettHangyak = megevetthangyak;
		_utirany = irany;
		_ehesseg = ehesseg;
		LeptetoLista.getInstance().add(this);

		Formazo.getInstance().visszateresSzoveg("none");
	}

	/** ez a fuggeny implementalja a hangyaszsun 
	* lepesenkent vegrehajtott cselekedeteinek az algoritmusat
	*/
	public void leptet() {
		Formazo.getInstance().hivasSzoveg("Hangyaszsun.leptet()");
		
	for(Mezo m : _mezok){
			

			
			//Minden iranyba megnezzuk a szomszedokat, utkozes, terites, sebzesre
			for(Irany irany : Irany.values()){
				Mezo szomszed = m.getSzomszed(irany);
				if(szomszed != null){ 
					
					
					
					//Lekerjuk a szomszed mezon tartozkodo jatekelemeket
					List<JatekElem> jatekElemLista = szomszed.getJatekElemLista();
					
					//Vegig megyunk ezeken
					for(JatekElem jatekElem : jatekElemLista){
						if(jatekElem != null){
							//Ha valaki erzekeny a hangya-ra, azt megsebezzuk
							for(Erzekeny e : jatekElem.getErzekenysegLista()){
								if(e.equals(Erzekeny.HANGYASZSUN)){
									jatekElem.sebez(200);
									_megevettHangyak++;
								}
							}
						}
					}
				} else	{ 	//szomszed == null 

				}
			}//irany : Irany
			
		
		}
	
		
		
		//ha nem tudunk elmozogni az utiranyba vegig nezzuk az osszes tobbin
		if(mozogniProbalIrany(_utirany) == false){
			
			for(Irany i : Irany.values()){
				if(i.equals(_utirany) == false){
					if(mozogniProbalIrany(i)== true){
						//ha sikerult elmozdulni valamerre vegeztunk
						_utirany = i;
						break;
					}
				}
			}
			
		}
		
		if(_megevettHangyak >= _ehesseg){
			
			isHalott = true;
		}
		
		
		
	
	

		Formazo.getInstance().visszateresSzoveg("void");
	}
	
	@Override
	public void halal(){
		LeptetoLista.getInstance().remove(this);
		TerepRajzoloLista.instance().removeRajzEgyseg(rajzEgyseg);
	}
	
	private boolean mozogniProbalIrany(Irany menet){
		Mezo szomszed = _mezok.get(0);
		
		if(szomszed != null && kovSzomszedUtkozes(menet,szomszed) == true){
			//van kavics a kovetkezo mezon, meg tudunk tolni
			Mezo kovmezo = szomszed.getSzomszed(menet);
			
			if(kovmezo != null&& kovSzomszedUtkozes(menet,kovmezo)){
				//van kavics a kovetkezo utani mezon is, meg tudunk tolni
				Mezo kovkovmezo = kovmezo.getSzomszed(menet);
				if(kovkovmezo != null && kovSzomszedUtkozes(menet,kovkovmezo)){
					//van kavics a kovetkezo utani utani mezon is, mar nem tudunk tolni se mozogni
					return false;
				} else {
					//masodik kavics tolasa
					if(kovkovmezo != null){
					mozgatKavicsok(kovkovmezo,menet);
					}
					//elso kavics tolasa
					if(kovmezo!=null){
					mozgatKavicsok(kovmezo,menet);
					}
				}
				
			}  else {
				//elso kavics tolasas
				if(kovmezo != null){
				mozgatKavicsok(kovmezo,menet);
				}
			}
		}
		//ha tudtunk tolni, vagy nem volt utkozes
		if(kovSzomszedUtkozes(menet, _mezok.get(0))==false){
			
			mozgasIranyba(menet);
			return true;
		} else{
			return false;
		}
	}
	
	private void mozgatKavicsok(Mezo m,Irany menet){
		if(m.getSzomszed(menet) != null){
			for(int i = 0;i<m.getJatekElemLista().size();i++){
				JatekElem je = m.getJatekElemLista().get(i);
				if(je.vanUtkozes()){
					je.mozgasIranyba(menet);
				}
			}
		}
	}
	
	private boolean kovSzomszedUtkozes(Irany menet,Mezo honnan){
		Mezo kov_szomszed = honnan.getSzomszed(menet);
		
		if(kov_szomszed == null) return true;
		for(JatekElem je : kov_szomszed.getJatekElemLista())
		{
			if(je.vanUtkozes() == true) return true;
			
		}
		return false;
	}
	
	@Override
	public void mentes(File out){
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("HANGYASZSUN"+" "+_elet+" "+_megevettHangyak+" "+_ehesseg+" "+_utirany);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void mentes(int x,int y,File out){
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("SET "+x+" "+y+" HANGYASZSUN"+" "+_elet+" "+_megevettHangyak+" "+_ehesseg+" "+_utirany);
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
