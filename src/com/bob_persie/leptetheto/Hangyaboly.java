package com.bob_persie.leptetheto;

import java.awt.Point;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import com.bob_persie.JatekElem;
import com.bob_persie.Mezo;
import com.bob_persie.Mezo.Irany;
import com.bob_persie.grafikus.TerepRajzoloLista;
import com.bob_persie.grafikus.rajzegysegek.HangyaRajzEgyseg;
import com.bob_persie.interfaces.Lepes;
import com.bob_persie.jatekkeret.LeptetoLista;
import com.bob_persie.utility.Formazo;

/**
 * Hangyaboly nevű játékelem implementációja raktározza az általa
 * kibocsájtott hangyák számát és a pályán lévő táplálékok számát
 */
public class Hangyaboly extends JatekElem implements Lepes {
	private int _hangyaKibocsatas = 5;
	private int _taplalek_szam = 0;
	private int _lepes_szam = 0;

	/**
	 * konstruktorban hozzaadjuk a hangyabolyt a leptetendo jatekelemek
	 * listajahoz
	 */
	public Hangyaboly() {
		Formazo.getInstance().hivasSzoveg("Hangyaboly()");
		LeptetoLista.getInstance().add(this);
		Formazo.getInstance().visszateresSzoveg("none");

	}

	/**
	 * konstruktorban hozzaadjuk a hangyabolyt a leptetendo jatekelemek
	 * listajahoz
	 * 
	 * @param hangyaKibocsatas
	 *            - a hangyak maximalis szama
	 */
	public Hangyaboly(int hangyaKibocsatas) {
		Formazo.getInstance().hivasSzoveg("Hangyaboly(hangyaKibocsatas)");
		LeptetoLista.getInstance().add(this);
		_hangyaKibocsatas = hangyaKibocsatas;

		Formazo.getInstance().visszateresSzoveg("none");
	}

	public Hangyaboly(int elet, int hkibocs, int tapszam) {
		Formazo.getInstance().hivasSzoveg("Hangyaboly(hangyaKibocsatas)");
		LeptetoLista.getInstance().add(this);
		_elet = elet;
		_hangyaKibocsatas = hkibocs;
		_taplalek_szam = tapszam;
		Formazo.getInstance().visszateresSzoveg("none");
	}

	/**
	 * ez a fuggveny implementalja a hangyaboly lepesenkent vegrehajtott
	 * cselekedeteinek az algoritmusat
	 */
	public void leptet() {
		Formazo.getInstance().hivasSzoveg("Hangyaboly.leptet()");

		/**
		 * megnezi, hogy az o mezojen milyen jatekelemek vannak es lekeri, hogy
		 * van-e naluk taplalek ha van akkor azt elraktarozza
		 */
		for (Mezo m : _mezok) {
			for (JatekElem j : m.getJatekElemLista()) {
				if (j.vanTaplalek()) {
					_taplalek_szam++;
				}
			}
		}

		_lepes_szam++;
		if (_lepes_szam >= _hangyaKibocsatas) {
			
			hangyaSpawnolas();
			_lepes_szam=0;
		}

		Formazo.getInstance().visszateresSzoveg("void");
	}

	private void hangyaSpawnolas() {
		
		
		for(Mezo m : _mezok){
			Map<Irany,Boolean> szomszedok_utkozes = Collections.synchronizedMap(new EnumMap<Irany,Boolean>(Irany.class));
			//Minden iranyba megnezzuk a szomszedokat, utkozes
			for(Irany irany : Irany.values()){
				Mezo szomszed = m.getSzomszed(irany);
				if(szomszed != null){ 
					szomszedok_utkozes.put(irany, false);
					
					List<JatekElem> jatekElemLista = szomszed.getJatekElemLista();
					
					for(JatekElem jatekElem : jatekElemLista){
						if(jatekElem != null){
							if(jatekElem.vanUtkozes()){
								szomszedok_utkozes.put(irany, true);
							}
						}
					}
				} else {
					szomszedok_utkozes.put(irany, true);
				}
			}
		
			//megvizsgaljuk van-e szomszedos mezo ahova letudunk rakni hangya-t
			boolean vanLehetoseg = false;
			Irany spawn = Irany.K;
			for(Irany i : Irany.values()){
				if(szomszedok_utkozes.get(i) == false){
					
					vanLehetoseg = true;
					spawn = i;
				}
			}
			//ha van, le is rakunk
			if(vanLehetoseg){
				putHangya(m,spawn);
			}
		}
				
		
	}

	private void putHangya(Mezo m, Irany spawn) {
		
		//Hangya kre�l�s
		Hangya h = new Hangya(100,false,Irany.K,0);
		
		//Mezo hozzaad�sa
		m.addJatekElem(h);
		h.addMezo(m);
		
		//kiszamitjuk a poziciojat
		Point p = Mezo.getPointSzomszed(rajzEgyseg.getPosition(), spawn);

		//hangyaRajz egyseg krealasa
		HangyaRajzEgyseg hre = new HangyaRajzEgyseg(Irany.K,p);
		h.setRajzEgyseg(hre);
		
		TerepRajzoloLista.instance().addRajzEgyseg(hre);
		
		
		
		
	}

	@Override
	public void mentes(File out) {
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("HANGYABOLY" + " " + _elet + " " + _hangyaKibocsatas
					+ " " + _taplalek_szam);

			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void mentes(int x,int y,File out) {
		try {
			PrintWriter output = new PrintWriter(new FileWriter(out, true));
			output.println("SET "+x+" "+y+" HANGYABOLY" + " " + _elet + " " + _hangyaKibocsatas
					+ " " + _taplalek_szam);

			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
